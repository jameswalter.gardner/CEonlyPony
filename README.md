# CEonlyPony
*Analysing the various cases for a gravitational-wave detector in Australia. Including the science case for Cosmic Explorer (CE) operating alone (without any 2nd generation or Einstein Telescope detectors).*

James Gardner, 2022

Current build found on the LIGO GitLab available [here](https://git.ligo.org/jameswalter.gardner/CEonlyPony). This is based off the old repository on [GitHub](https://github.com/daccordeon/CEonlyPony).

---
- Contact james.gardner (at) anu.edu.au for any technical enquiries.
- Python requirements for the codebase in `./source/` found in `./requirements.txt`.
- The critical path to reproduce the results is explained in `./source/guide_to_reproducing_results.ipynb`.
- The figures from the paper can be produced by executing `python3 ./source/paper_figures_source/plot_all_submission_figures.py`.
