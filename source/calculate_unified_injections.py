"""Calculates the same set of injections for a set of networks.

Based on the old calculate_injections.py and gwbench's multi_network.py example script, this processes the injections (e.g. in data_raw_injections/) in union for each network in a set and saves the results (e.g. in data_processed_injections/). This is faster than the previous implementation if detectors are shared between the networks because the detector responses are only calculated once.

Usage:
    See the example in run_unified_injections.py.

License:
    BSD 3-Clause License

    Copyright (c) 2022, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
from typing import List, Set, Dict, Tuple, Optional, Union
import os
import numpy as np
import pandas as pd
from pprint import pprint

from gwbench import network
from gwbench.basic_relations import f_isco_Msolar, m1_m2_of_Mc_eta, M_of_Mc_eta

from useful_functions import (
    without_rows_w_nan,
    parallel_map,
    HiddenPrints,
    PassEnterExit,
    row_iter_from_dataframe,
)
from network_subclass import NetworkExtended


def fisco_obs_from_Mc_eta(
    Mc: float, eta: float, redshifted: bool = True, z: Optional[float] = None
) -> float:
    """Returns the frequency of the ISCO in the observer's frame.

    Formula: fisco_obs = (6**1.5*PI*(1+z)*Mtot_source)**-1
    Missing some number of Msun, c=1, G=1 factors.

    Args:
        Mc: Chirp mass.
        eta: Symmetric mass ratio.
        redshifted: Whether the masses are already redshifted inside gwbench.
        z: Redshift, required to redshift the masses.

    Raises:
        ValueError: If redshifted is false but redshift (z) isn't given.
    """
    Mtot = M_of_Mc_eta(Mc, eta)  # Mc / eta**0.6
    if redshifted:
        return f_isco_Msolar(Mtot)
    else:
        if z is None:
            raise ValueError("redshifted is False, provide z value to redshift now")
        return f_isco_Msolar((1.0 + z) * Mtot)


def multi_network_results_for_injection(
    network_specs: List[List[str]],
    inj: Dict[str, Union[int, float]],
    base_params: Dict[str, Union[int, float]],
    wf_dict: Dict[str, Union[str, Optional[Dict[str, str]], bool, int]],
    deriv_dict: Dict[
        str,
        Union[
            str,
            Tuple[str, ...],
            List[Set[str]],
            bool,
            Optional[Dict[str, Union[float, str, int]]],
        ],
    ],
    misc_settings_dict: Dict[str, Optional[int]],
    debug: bool = False,
) -> Dict[str, Dict[str, Union[int, bool, str, float]]]:
    """Returns the benchmark as a dict of tuples for a single injection using the inj and base_params and the settings dicts through the networks in network_specs.

    DISABLED FEATURE (now this is only done before resampling): If a single network fails an injection due to FIM, then the unified results will save it as a np.nan in all networks so that the universe of injections is the same between each network. TODO: check that this doesn't bias the results away from loud sources that we care about.

    Args:
        network_specs: Networks to pass to gwbench's multi-network pipeline.
        inj: Injection parameters for each injection, e.g. injection index, chirp mass, and luminosity distance, with correct types loaded from the raw injections .h5 data file.
        base_params: Common parameters among injections, e.g. time of coalesence.
        wf_dict: Waveform dictionary of model name and options, also contains the science case string.
        deriv_dict: Derivative options dictionary.
        misc_settings_dict: Options for gwbench, e.g. whether to account for Earth's rotation about its axis.
        debug: Whether to print debug statements

    Returns:
        Dict[str, Dict[str, Union[int, bool, str, float]]]: Keys are repr(network_spec). Each value is a dictionary with keys ["injection_index", "success_bool", "rejection_flag", "unified_success_bool", "redshift", "snr", "err_logMc", "err_logDL", "err_eta", "err_iota", "sky_area_90"] to become a row in a pandas dataframe and saved as .h5. If the injection failed either due to ill-conditioning or fISCO being too low, then the injection is not discarded, success_bool will be False, the rejection flag will state whether it was rejected due to ill-conditioning or due to fISCO being too low, and the last seven parameters will be np.nan's. If the injection fails in another network, then the unified success bool will be False.
    """
    # don't strictly need to remove variables from parameters dictionary as gwbench will ignore unrecognised keys
    injection_index = inj.pop("injection_index")
    z = inj.pop("z")
    inj_params = dict(**base_params, **inj)
    if debug:
        pprint(
            (
                f"Injections parameters for injection index: {injection_index}",
                inj_params,
            )
        )

    # if V+ (aLIGO) is present in any network, then frequency_array is truncated for all networks (since it is shared below). TODO: figure out how common this is
    aLIGO_or_Vplus_used = ("aLIGO" in deriv_dict["unique_tecs"]) or (
        "V+" in deriv_dict["unique_tecs"]
    )
    fmin, fmax = 5.0, wf_dict["coeff_fisco"] * fisco_obs_from_Mc_eta(
        inj_params["Mc"],
        inj_params["eta"],
        redshifted=misc_settings_dict["redshifted"],
        z=z,
    )
    # if wf_dict contains specified frequencies to override, only apply if it restricts the range (and fmax is made safe for certain PSDs below)
    if "fmin" in wf_dict.keys():
        fmin = max(fmin, wf_dict["fmin"])
    if "fmax" in wf_dict.keys():
        fmax = min(fmax, wf_dict["fmax"])
    # chosing fmax in 11 <= coeff_fisco*fisco <= 1024, truncating to boundary values, NB: B&S2022 doesn't include the lower bound which must be included to avoid an IndexError with the automatically truncated fmin from the V+ and aLIGO curves stored in gwbench that start at 10 Hz, this can occur for Mtot > 3520 Msun with massive BBH mergers although those masses are at least an order of magnitude beyond any observed so far
    if aLIGO_or_Vplus_used:
        fmax_bounds = (11, 1024)
    else:
        fmax_bounds = (6, 1024)
    fmax = float(max(min(fmax, fmax_bounds[1]), fmax_bounds[0]))
    # before and at ringdown the waveform is far from monochromatic and needs dense sampling in frequency
    df = 1 / 16.0
    frequency_array = np.arange(fmin, fmax + df, df)
    if debug:
        print(f"frequency array: {frequency_array}, waveform user variables: {wf_dict}")

    # passing parameters to gwbench, hide stdout (i.e. prints) if not debugging, stderr should still show up
    if not debug:
        entry_class = HiddenPrints
    else:
        entry_class = PassEnterExit
    with entry_class():
        # precalculate the unique components (detector derivatives and PSDs) common among all networks
        # calculate the unique detector response derivatives
        loc_net_args = (
            network_specs,
            frequency_array,
            inj_params,
            deriv_dict["deriv_symbs_string"],
            wf_dict["wf_model_name"],
            wf_dict["wf_other_var_dic"],
            deriv_dict["conv_cos"],
            deriv_dict["conv_log"],
            misc_settings_dict["use_rot"],
            misc_settings_dict["num_cores"],
        )
        if not deriv_dict["numerical_over_symbolic_derivs"]:
            unique_loc_net = network.unique_locs_det_responses(*loc_net_args)
        else:
            # update eta if too close to its maximum value for current step size, https://en.wikipedia.org/wiki/Chirp_mass#Definition_from_component_masses; B&S instead discard these injections
            eta_max = 0.25
            deriv_dict["numerical_deriv_settings"]["step"] = min(
                deriv_dict["numerical_deriv_settings"]["step"],
                (eta_max - inj_params["eta"]) / 10.0,
            )
            unique_loc_net = network.unique_locs_det_responses(
                *loc_net_args,
                deriv_dict["numerical_deriv_settings"]["step"],
                deriv_dict["numerical_deriv_settings"]["method"],
                deriv_dict["numerical_deriv_settings"]["order"],
                deriv_dict["numerical_deriv_settings"]["n"],
            )
        # get the unique PSDs for the various detector technologies
        unique_tec_net = network.unique_tecs(network_specs, frequency_array)

        # perform the analysis of each network from the unique components
        multi_network_results_dict = dict()
        for i, network_spec in enumerate(network_specs):
            # reject if BBH and fISCO is too low (i.e fmax is too close to fmin)
            # if BBH, then discard the injection by returning NaNs if fmax < 12 Hz (7 Hz) for aLIGO or V+ (everything else).
            tecs_local = [det_spec.split("_")[0] for det_spec in network_spec]
            aLIGO_or_Vplus_used_local = "aLIGO" in tecs_local or "V+" in tecs_local
            # changing to 8 and 11 to match Ssohrab - 23 May 2022
            if (wf_dict["science_case"] == "BBH") and (
                (fmax < 8) or (aLIGO_or_Vplus_used_local and (fmax < 11))
            ):
                if debug:
                    print(
                        f"Rejecting BBH injection {injection_index} for high redshifted masses: (fmax, redshifted) = {fmax, misc_settings_dict['redshifted']}."
                    )
                multi_network_results_dict[repr(network_spec)] = {
                    "injection_index": injection_index,
                    "success_bool": False,
                    "rejection_flag": "rejected_for_low_fISCO",
                    "unified_success_bool": None,
                    "redshift": np.nan,
                    "snr": np.nan,
                    "err_logMc": np.nan,
                    "err_logDL": np.nan,
                    "err_eta": np.nan,
                    "err_iota": np.nan,
                    "sky_area_90": np.nan,
                }
                continue

            # if net only exists here, then the subclass is pointless. re-factor to justify using subclass
            net = network.Network(network_spec)
            # get the correct network from the unique components calculated above. this avoids the need to .set_net_vars, .set_wf_vars, .setup_ant_pat_lpf_psds, .calc_det_responses, .calc_det_responses_derivs_num/sym,
            net.get_det_responses_psds_from_locs_tecs(unique_loc_net, unique_tec_net)
            # calculate the network SNRs
            net.calc_snrs(only_net=misc_settings_dict["only_net"])
            # calculate the Fisher and covariance matrices, then error estimates
            net.calc_errors(only_net=misc_settings_dict["only_net"])
            # calculate the 90%-credible sky area (in [deg]^2)
            net.calc_sky_area_90(only_net=misc_settings_dict["only_net"])

            # TODO: if using gwbench 0.7, still introduce a limit on net.cond_num based on machine precision errors that mpmath is blind to
            # if the FIM is zero, then the condition number is NaN and matrix is ill-conditioned (according to gwbench). TODO: try catching this by converting warnings to errors following <https://stackoverflow.com/questions/5644836/in-python-how-does-one-catch-warnings-as-if-they-were-exceptions#30368735> --> 54 and 154 converged in a second run
            if debug:
                pprint(
                    (
                        f"For {network_spec}",
                        f"Condition number: {net.cond_num}",
                        "Covariance matrix: ",
                        net.cov,
                        "Errors: ",
                        net.errs,
                    )
                )
            if not net.wc_fisher:
                # unified_success_bool allows for later unified injection rejection so that cosmological resampling can be uniform across networks, it means that the number of uniformly rejected injections is equal to that of the weakest network in the set but leads to a better comparison
                if debug:
                    print(
                        f"Rejected injection {injection_index} for {network_spec} and, therefore, all networks in the multi-network because of ill-conditioned FIM ({net.fisher}) with condition number ({net.cond_num}) greater than 1e15"
                    )
                multi_network_results_dict[repr(network_spec)] = {
                    "injection_index": injection_index,
                    "success_bool": False,
                    "rejection_flag": "rejected_for_ill-conditioned_FIM",
                    "unified_success_bool": None,
                    "redshift": np.nan,
                    "snr": np.nan,
                    "err_logMc": np.nan,
                    "err_logDL": np.nan,
                    "err_eta": np.nan,
                    "err_iota": np.nan,
                    "sky_area_90": np.nan,
                }
            else:
                err_dict = dict()
                # full deriv_dict['deriv_symbs_string'] = Mc eta DL tc phic iota ra dec psi
                translate_dict = dict(
                    log_Mc="Mc",
                    log_DL="DL",
                    eta="eta",
                    cos_iota="iota",
                    sky_area_90="ra",
                )
                for err_key in ["log_Mc", "log_DL", "eta", "cos_iota", "sky_area_90"]:
                    if translate_dict[err_key] in deriv_dict["deriv_symbs_string"]:
                        err_dict[err_key] = net.errs[err_key]
                    else:
                        err_dict[err_key] = np.nan

                # convert sigma_cos(iota) into sigma_iota
                abs_err_iota = abs(err_dict["cos_iota"] / np.sin(inj_params["iota"]))
                multi_network_results_dict[repr(network_spec)] = {
                    "injection_index": injection_index,
                    "success_bool": True,
                    "rejection_flag": "not_rejected",
                    "unified_success_bool": None,
                    "redshift": z,
                    "snr": net.snr,
                    "err_logMc": err_dict["log_Mc"],
                    "err_logDL": err_dict["log_DL"],
                    "err_eta": err_dict["eta"],
                    "err_iota": abs_err_iota,
                    "sky_area_90": err_dict["sky_area_90"],
                }

    # whether the injection succeeded in all networks
    unified_success_bool = all(
        output["success_bool"] for output in multi_network_results_dict.values()
    )
    for output in multi_network_results_dict.values():
        output["unified_success_bool"] = unified_success_bool
    if debug:
        pprint(
            (
                "All settings: waveform: ",
                wf_dict,
                "derivatives: ",
                deriv_dict,
                "miscellaneous: ",
                misc_settings_dict,
                "Select settings to network.unique_loc_net: ",
                loc_net_args,
                "Results: ",
                multi_network_results_dict,
            )
        )
    return multi_network_results_dict


def multi_network_results_for_injections_file(
    results_file_name: str,
    network_specs: List[List[str]],
    injections_file: str,
    num_injs_per_redshift_bin: int,
    process_injs_per_task: Optional[int],
    base_params: Dict[str, Union[int, float]],
    wf_dict: Dict[str, Union[str, Optional[Dict[str, str]], bool, int]],
    deriv_dict: Dict[
        str,
        Union[
            str,
            Tuple[str, ...],
            List[Set[str]],
            bool,
            Optional[Dict[str, Union[float, str, int]]],
        ],
    ],
    misc_settings_dict: Dict[str, Optional[int]],
    data_path: str = "./data_processed_injections/task_files/",
    debug: int = False,
) -> None:
    """Runs the injections in the given file through the given set of networks and saves them as a .h5 file.

    Benchmarks the first process_injs_per_task number of injections from injections_file + base_params for each of the networks in network_specs for the science_case and other settings in the three dict.'s provided, saves the results as a pandas dataframe in a .h5 file in results_file_name at data_path with columns to label the injection, note whether the benchmarking succeeded, and for the results of the benchmark: redshift, SNR, logMc error, logDL error, eta error, iota error, and 90%-credible sky-area in sqr degrees.

    Args:
        results_file_name: Output .h5 filename template for each of the network results. Of the form f"SLURM_TASK_{task_id}" if to be generated automatically later. TODO: check whether this works without the task_id format.
        network_specs: Set of networks to analyse.
        injections_file: Input injections filename with path.
        num_injs_per_redshift_bin: Total number of injections from the injections file across all tasks (used for labelling).
        process_injs_per_task: Number of injections to process, does all of them if None.
        base_params: Common parameters among injections, e.g. time of coalesence.
        wf_dict: Waveform dictionary of model name and options, also contains the science case string.
        deriv_dict: Derivative options dictionary.
        misc_settings_dict: Options for gwbench, e.g. whether to account for Earth's rotation about its axis.
        data_path: Path to the output processed data file for the task.
        debug: Whether to print debug statements

    Raises:
        Exception: If any of the target results files already exist.
    """
    injections_df = pd.read_hdf(injections_file)
    if process_injs_per_task is None:
        process_injs_per_task = len(injections_df)
    # only process the first process_injs_per_task of labelled_inj_data
    process_injections_df = injections_df.iloc[:process_injs_per_task]

    # check if results files already exist
    # can't pass net_copy because of memory constraints, want to stay low (200 MB), TODO: test if this actually affects scheduling
    results_file_name_list = []
    for network_spec in network_specs:
        net = NetworkExtended(
            network_spec,
            wf_dict["science_case"],
            wf_dict["wf_model_name"],
            wf_dict["wf_other_var_dic"],
            num_injs_per_redshift_bin,
            file_name=results_file_name,
            data_path=data_path,
        )
        # includes path, injs-per-zbin is num_injs_per_redshift_bin input to generate_injections (e.g. will be 250k)
        results_file_name_list.append(net.file_name_with_path)
        if debug:
            print(net.file_name_with_path)
        if net.results_file_exists:
            raise Exception("Some results file/s already exist, aborting process.")

    # list of multi_network_results_dict's from each injection
    multi_network_results_dict_list = parallel_map(
        lambda injection_dict: multi_network_results_for_injection(
            network_specs,
            injection_dict,
            base_params,
            wf_dict,
            deriv_dict,
            misc_settings_dict,
            debug=debug,
        ),
        row_iter_from_dataframe(process_injections_df, return_type="dict"),
        parallel=misc_settings_dict["num_cores"] is not None,
        num_cpus=misc_settings_dict["num_cores"],
    )

    # build dataframes out of results for each network
    for i, network_spec in enumerate(network_specs):
        results = [
            multi_network_results_dict[repr(network_spec)]
            for multi_network_results_dict in multi_network_results_dict_list
        ]
        df = pd.DataFrame(results)
        # save in pytables format to allow appending, needs min_itemsize to avoid unwanted truncation of string columns; could even store other attributes in the hdf5 file under other keys rather than being inferred by results class, TODO: consider this.
        df.to_hdf(
            results_file_name_list[i],
            key="df",
            format="table",
            min_itemsize={"rejection_flag": 40},
        )
