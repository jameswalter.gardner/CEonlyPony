"""Plot combined (loose and tight) metric hat graphs for paper.

Usage:
    When in source/ directory for relative paths to be correct:
    $ python3 paper_figures_source/plot_combined_metric_hat_graphs.py

License:
    BSD 3-Clause License

    Copyright (c) 2023, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from typing import List, Set, Dict, Tuple, Optional, Union, Callable
from numpy.typing import NDArray

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

# change to LaTeX style fonts
import matplotlib

matplotlib.rcParams["mathtext.fontset"] = "stix"
matplotlib.rcParams["font.family"] = "STIXGeneral"

# include parent directory in import path
import sys, os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

# import psd loader from parent directory and remember to update import path for data files below
from gwbench.psd import psd

from useful_plotting_functions import plot_hat_graph, BANG_WONG_COLOURS
from summary_table import summary_table
from filesystem_interaction import *

sensitivity_files_path = (
    "./paper_figures_source/data_files/sensitivity_curves_TXT_files"
)
tables_path = "./paper_figures_source/data_files/summary_tables_H5_files/reliable/"
legend_kwargs = dict()

subset_plot_options = dict(
    plot_fraction_in_range=True,
    display_precision=0,
    use_percentage=True,
)
unification_level = "unified_with_all_multiple_detector_runs"
input_dir = tables_path

metric_settings_dict = dict(
    loose=dict(
        reliable_metric="SNR > 25, sky < 10",
        truncated_ylims=(
            0.3,
            1,
        ),
        # ylabel=r"fractional loose follow-up metric",
        ylabel=r"fraction of sources localised within $10\,\mathrm{deg}^2$",
        hspace=0.5,
    ),
    tight=dict(
        reliable_metric="SNR > 25, sky < 1",
        truncated_ylims=(0.2, 0.9),
        # ylabel=r"fractional tight follow-up metric",
        ylabel=r"fraction of sources localised within $1\,\mathrm{deg}^2$",
        hspace=0.63,
    ),
)
y1, y2 = metric_settings_dict["loose"]["truncated_ylims"]
y3, y4 = metric_settings_dict["tight"]["truncated_ylims"]
y1max, y2max = (
    max(y1, y3),
    max(y2, y4),
)

# what AU networks to show
settings_networks_shown = dict(
    main_text=dict(
        no_reference_network=False,
        non_NEMO=True,
        NEMO=False,
    ),
    appendix_nemo=dict(
        no_reference_network=False,
        non_NEMO=False,
        NEMO=True,
    ),
)

user_selection_of_which_pair_of_plots_dict = dict(
    main_text=dict(
        plots=[
            dict(
                **settings_networks_shown["main_text"], **metric_settings_dict["loose"]
            ),
            dict(
                **settings_networks_shown["main_text"], **metric_settings_dict["tight"]
            ),
        ],
        output_plot_label="fig2.pdf",
    ),
    appendix=dict(
        plots=[
            dict(
                **settings_networks_shown["appendix_nemo"],
                **metric_settings_dict["loose"],
            ),
            dict(
                **settings_networks_shown["appendix_nemo"],
                **metric_settings_dict["tight"],
            ),
        ],
        output_plot_label="fig4.pdf",
    ),
)

# disable other plotting to tune the inset
disable_for_inset = False

for user_selection, pair_of_plots in user_selection_of_which_pair_of_plots_dict.items():
    if disable_for_inset and user_selection != "appendix":
        continue

    output_plot_label = pair_of_plots["output_plot_label"]

    plt.rcParams.update({"font.size": 22})
    fig, axs = plt.subplots(
        nrows=2,
        ncols=2,
        # sharey="row",
        figsize=(9.5 * 2.2, 9.5 * (y1max + y2max) / (0.3 + 1)),
        gridspec_kw={
            "height_ratios": [y1max, y2max],
            # "hspace": 0.55,
            "hspace": 0.65,
            "wspace": 0.25,
        },
    )

    for col_index, settings_dict in enumerate(pair_of_plots["plots"]):
        # TODO: figure out a more efficient way to unpack the settings
        reliable_metric = settings_dict["reliable_metric"]
        truncated_ylim1, truncated_ylim2 = settings_dict["truncated_ylims"]
        ylabel = settings_dict["ylabel"]
        hspace = settings_dict["hspace"]
        no_reference_network = settings_dict["no_reference_network"]
        non_NEMO = settings_dict["non_NEMO"]
        NEMO = settings_dict["NEMO"]

        # general kwargs for plotting each generation
        generation_plot_kwargs_dict = {
            "2HALFG": dict(
                kwargs_for_loading_table=dict(
                    metric=reliable_metric,
                    reorder=(3, 2, 1, 0),
                    network_labels_plot=[
                        # "Voy_H,\nVoy_L,\nV+_V,\nK+_K,\nVoy_I",
                        # "Voy_H,\nVoy_L,\nV+_V,\nK+_K",
                        # "Voy_H,\nVoy_L,\nV+_V",
                        # "Voy_H,\nVoy_L",
                        "Voyager (H)\nVoyager (L)\nVirgo+\nKAGRA+\nVoyager (I)",
                        "Voyager (H)\nVoyager (L)\nVirgo+\nKAGRA+",
                        "Voyager (H)\nVoyager (L)\nVirgo+",
                        "Voyager (H)\nVoyager (L)",
                    ],
                    plot_file_tag="2HALFG",
                    hat_data_indices=[
                        (2, None, 3),
                        # (0, None, 3),
                        # (1, None, 3)
                    ],
                ),
                kwargs_for_save_hat_graph=dict(
                    australian_labels=[
                        "no observatory (AU)",
                        # r"A#-$f^{-6}$_AU",
                        # "NEMO_AU",
                    ],
                    colour_list=[
                        None,
                        # "r",
                        # "c"
                    ],
                    legend_kwargs=legend_kwargs,
                    # annotation_fontsize=16,
                    annotation_fontsize=None,
                    abbreviate_lambda_label=True,
                ),
            ),
            "3G": dict(
                kwargs_for_loading_table=dict(
                    metric=reliable_metric,
                    reorder=(1, 3, 0, 2),
                    network_labels_plot=[
                        # "CE-40-CBO_C,\nCE-20-CBO_N",
                        # "CE-40-CBO_C",
                        # "ET_E,\nCE-40-CBO_C",
                        # "ET_E",
                        # "CE 40-km (CBO; C)\nCE 20-km (CBO; N)",
                        # "CE 40-km (CBO; C)",
                        # "ET\nCE 40-km (CBO; C)",
                        # "ET",
                        # "CE 40-km (C)\nCE 20-km (N)",
                        # "CE 40-km (C)",
                        # "ET\nCE 40-km (C)",
                        # "ET",
                        "CE 40 km (C)\nCE 20 km (N)",
                        "CE 40 km (C)",
                        "ET\nCE 40 km (C)",
                        "ET",
                    ],
                    plot_file_tag="3G",
                    plot_file_tag_submission="fig4",
                    hat_data_indices=[
                        (2, None, 4),
                        # (3, None, 4),
                        # (1, None, 4),
                        # (0, None, 4),
                    ],
                ),
                kwargs_for_save_hat_graph=dict(
                    australian_labels=[
                        "no observatory (AU)",
                        # "NEMO_AU",
                        # "CE-20-PMO_AU",
                        # "CE-20-CBO_AU",
                    ],
                    colour_list=[
                        None,
                        # "c",
                        # "g",
                        # "m"
                    ],
                    # legend_kwargs=dict(fontsize=19),
                    legend_kwargs=dict(fontsize=22),
                    # annotation_fontsize=16 if user_selection == "appendix" else 12,
                    annotation_fontsize=None,
                    abbreviate_lambda_label=True,
                ),
            ),
        }

        # no_reference_network existing detectors if only going to show NEMO
        if no_reference_network:
            for _, generation_plot_kwargs in generation_plot_kwargs_dict.items():
                generation_plot_kwargs["kwargs_for_save_hat_graph"][
                    "australian_labels"
                ] = []
                generation_plot_kwargs["kwargs_for_save_hat_graph"]["colour_list"] = []
                generation_plot_kwargs["kwargs_for_loading_table"][
                    "hat_data_indices"
                ] = []

        # labels
        # australian_labels = [r"$\mathrm{A}\#_\mathrm{3Sus}$_AU", "CE-20-PMO_AU", "CE-20-CBO_AU", "NEMO_AU"]
        australian_labels = [
            r"$\mathrm{A}\#_\mathrm{3Sus}$ (AU)",
            # "CE 20-km (PMO; AU)",
            # "CE 20-km (CBO; AU)",
            "CE 20 km (PMO; AU)",
            "CE 20 km (CBO; AU)",
            "NEMO (AU)",
        ]

        # add A# and CEs to plot settings
        if non_NEMO:
            generation_plot_kwargs_dict["2HALFG"]["kwargs_for_save_hat_graph"][
                "australian_labels"
            ].append(australian_labels[0])
            generation_plot_kwargs_dict["2HALFG"]["kwargs_for_save_hat_graph"][
                "colour_list"
            ].append(BANG_WONG_COLOURS["Vermillion"])
            generation_plot_kwargs_dict["2HALFG"]["kwargs_for_loading_table"][
                "hat_data_indices"
            ].append((0, None, 3))

            generation_plot_kwargs_dict["3G"]["kwargs_for_save_hat_graph"][
                "australian_labels"
            ] += [*australian_labels[1:3]]
            generation_plot_kwargs_dict["3G"]["kwargs_for_save_hat_graph"][
                "colour_list"
            ] += [BANG_WONG_COLOURS["Bluish green"], BANG_WONG_COLOURS["Reddish purple"]]
            generation_plot_kwargs_dict["3G"]["kwargs_for_loading_table"][
                "hat_data_indices"
            ] += [(1, None, 4), (0, None, 4)]

        # add NEMO to plot settings
        if NEMO:
            generation_plot_kwargs_dict["2HALFG"]["kwargs_for_save_hat_graph"][
                "australian_labels"
            ].append(australian_labels[3])
            generation_plot_kwargs_dict["2HALFG"]["kwargs_for_save_hat_graph"][
                "colour_list"
            ].append(BANG_WONG_COLOURS["Sky blue"])
            generation_plot_kwargs_dict["2HALFG"]["kwargs_for_loading_table"][
                "hat_data_indices"
            ].append((1, None, 3))

            generation_plot_kwargs_dict["3G"]["kwargs_for_save_hat_graph"][
                "australian_labels"
            ].insert(1, australian_labels[3])
            generation_plot_kwargs_dict["3G"]["kwargs_for_save_hat_graph"][
                "colour_list"
            ].insert(1, BANG_WONG_COLOURS["Sky blue"])
            generation_plot_kwargs_dict["3G"]["kwargs_for_loading_table"][
                "hat_data_indices"
            ].insert(1, (3, None, 4))

        for i, (
            generation,
            generation_plot_kwargs,
        ) in enumerate(generation_plot_kwargs_dict.items()):
            input_path = f"{input_dir}{generation}.h5"

            source_counts_in_distance = {"in max range": 133.9}
            table = summary_table(
                save_path=input_path,
                return_table=True,
                display_table=False,
                save_html=False,
                reliability_thresholds=True,
            )

            metric = generation_plot_kwargs["kwargs_for_loading_table"]["metric"]
            # split into different Australian detectors
            hat_data = np.array(
                [
                    table[metric][i0:i1:i2]
                    for (i0, i1, i2) in generation_plot_kwargs[
                        "kwargs_for_loading_table"
                    ]["hat_data_indices"]
                ]
            )
            # re-order networks for pleasant plotting
            reorder = generation_plot_kwargs["kwargs_for_loading_table"]["reorder"]
            reorder_nets_for_plot = lambda itr: [itr[i] for i in reorder]
            hat_data = hat_data[:, reorder]

            plot_kwargs = dict(
                network_labels_plot=reorder_nets_for_plot(
                    generation_plot_kwargs["kwargs_for_loading_table"][
                        "network_labels_plot"
                    ]
                ),
                # reordered above
                hat_data=hat_data,
                source_counts_in_distance=source_counts_in_distance,
                #             output_path=output_path,
                **generation_plot_kwargs["kwargs_for_save_hat_graph"],
                display_precision=subset_plot_options["display_precision"],
                use_percentage=subset_plot_options["use_percentage"],
                plot_fraction_in_range=subset_plot_options["plot_fraction_in_range"],
            )

            #         output_path = output_path.replace(".pdf", "_fractional.pdf")
            hat_data /= source_counts_in_distance["in max range"]
            max_range = 1

            # TODO: functionalise the lambda calculation and bound
            # calculate the lambda values for each Australian detector hat
            # lists for baseline network_labels along x-axis
            debug = False
            abbreviate_lambda_label = generation_plot_kwargs[
                "kwargs_for_save_hat_graph"
            ]["abbreviate_lambda_label"]

            if debug:
                print(hat_data)
            baseline_data_list = hat_data[0]
            annotation_array = np.empty_like(hat_data, dtype=np.dtype(object))
            annotation_array[0] = ["" for _ in baseline_data_list]

            for j, australian_data_list in enumerate(hat_data[1:]):
                # additive change
                delta_list = australian_data_list - baseline_data_list
                # multiplicative change
                lambda_list = np.empty_like(baseline_data_list)
                label_list = np.empty_like(baseline_data_list, dtype=np.dtype(object))
                for k, baseline in enumerate(baseline_data_list):
                    # handling dividing by zero
                    if baseline == 0:
                        lambda_list[k] = np.nan
                        # finding a lower bound on lambda
                        observation_time_in_years = 10.0
                        # include a factor of 0.59 (updated to 0.68) as the resolution is set by the size of the unrejected population
                        # this also means that a uniform rejection rate will not affect the lambda bound
                        uniform_rejection_rate = 0.68
                        min_value_fraction = 1 / (
                            uniform_rejection_rate
                            * observation_time_in_years
                            * source_counts_in_distance["in max range"]
                        )
                        # print(source_counts_in_distance["in max range"])
                        lambda_bound = australian_data_list[k] / min_value_fraction
                        # TODO: import number of sources in range from somewhere
                        label_list[k] = r"$\lambda>$" + f"{lambda_bound:.0f}"
                    else:
                        lambda_list[k] = australian_data_list[k] / baseline
                        if lambda_list[k] > 10:
                            precision = 0
                        else:
                            precision = 1
                        label_list[k] = (
                            r"$\lambda=$" + f"{lambda_list[k]:.{precision}f}"
                        )
                    if abbreviate_lambda_label:
                        if debug:
                            print(label_list[k])
                        #                     label_list[k] = label_list[k][10:]
                        label_list[k] = (
                            label_list[k]
                            .replace("\\lambda", "")
                            .replace("=", "")
                            .replace("$", "")
                        )
                if debug:
                    print(label_list)
                annotation_array[j + 1] = label_list

                # export lambdas and bounds for separate plotting
                # df = pd.DataFrame(dict(lambda_list=lambda_list, label_list=label_list))
                # data_dir = (
                #     "./paper_figures_source/data_files/lambda_statistic_H5_files/"
                # )
                # save_hdf(df, data_dir + f"{j}.h5")

            if debug:
                print(annotation_array, "\n")

            # hack for submission
            add_asterix_for_single_detector_network = True if i == 1 else False

            # plot hat graph on ax
            # ax = axs[i]
            ax = axs[i, col_index]
            if not disable_for_inset:
                plot_hat_graph(
                    ax,
                    reorder_nets_for_plot(
                        generation_plot_kwargs["kwargs_for_loading_table"][
                            "network_labels_plot"
                        ]
                    ),
                    # already reordered above
                    hat_data,
                    group_labels=generation_plot_kwargs["kwargs_for_save_hat_graph"][
                        "australian_labels"
                    ],
                    **generation_plot_kwargs["kwargs_for_save_hat_graph"],
                    # turning this off to plot the lambda statistic separately
                    annotations_off=True,
                    spacing_between_hats=0.2,
                    annotation_array=annotation_array,
                    add_asterix_for_single_detector_network=add_asterix_for_single_detector_network,
                    # use stem graph instead
                    use_rectangles=False,
                )

            truncated_ylim = y1max if i == 0 else y2max
            ax.set_ylim(0, truncated_ylim * max_range)
            major_ticks = np.arange(0, 1 + 0.1, 0.2)
            ax.yaxis.set_ticks(major_ticks, minor=False)
            ax.yaxis.set_ticklabels(
                ["{:.0%}".format(tick) for tick in major_ticks], minor=False
            )
            minor_ticks = major_ticks[:-1] + 0.1
            ax.yaxis.set_ticks(minor_ticks, minor=True)
            ax.set_ylim(0, truncated_ylim * max_range)

            ax.grid(which="both", axis="y", color="gainsboro", zorder=0)

        axs[1, col_index].text(
            -0.13,
            0.3,
            ylabel,
            transform=axs[1, col_index].transAxes,
            rotation=90,
            rotation_mode="anchor",
        )

    legend_col = 0 if user_selection == "appendix" else 1
    if not disable_for_inset:
        handles0, labels0 = axs[0, legend_col].get_legend_handles_labels()
        handles, labels = axs[1, legend_col].get_legend_handles_labels()
        for handle, label in zip(handles0, labels0):
            # print(handle, label)
            if handle not in handles:
                handles.insert(0, handle)
            if label not in labels:
                labels.insert(0, label)
        # print(labels0, labels)
        axs[1, legend_col].legend(
            handles,
            labels,
            fontsize=22,
            loc="upper left",  # bbox_to_anchor=(0, 0.93)
        )

    axs[0, 0].text(0.01, 1.07, "(a)", transform=axs[0, 0].transAxes)
    axs[0, 1].text(0.01, 1.07, "(b)", transform=axs[0, 1].transAxes)
    axs[1, 0].text(0.01, 1.022, "(c)", transform=axs[1, 0].transAxes)
    axs[1, 1].text(0.01, 1.022, "(d)", transform=axs[1, 1].transAxes)

    # sensitivity curve inset
    if user_selection == "appendix":
        plt.rcParams.update({"font.size": 20})
        ax = fig.add_axes(
            [
                # bottom left corner in normalised coords
                0.64,
                0.28,
                # width and height
                0.15,
                0.2,
            ]
        )
        style_dict = {
            "NEMO": {"label": "NEMO", "color": BANG_WONG_COLOURS["Sky blue"], "linestyle": None, "linewidth": 3},
            "A#-100Hzf-6": {
                "label": r"$\mathrm{A}\#_\mathrm{3Sus}$",
                "color": BANG_WONG_COLOURS["Vermillion"],
                "linestyle": None,
                "linewidth": 3,
            },
        }
        frequency_array = np.geomspace(30, 5000, 200)
        flip_tuple_and_convert_to_asd = lambda tuple_psd_frequency: (
            tuple_psd_frequency[1],
            np.sqrt(tuple_psd_frequency[0]),
        )
        for tec, fmt_dict in style_dict.items():
            # using psd from gwbench.psd and updating to present sensitivity_files_path
            ax.loglog(
                *flip_tuple_and_convert_to_asd(
                    psd(tec, frequency_array, path=sensitivity_files_path)
                ),
                **fmt_dict,
            )
        ax.set(
            ylim=(9e-25, 1e-23),
            xlim=(min(frequency_array), max(frequency_array)),
        )
        ax.set_xlabel("frequency [Hz]")  # , fontsize=12)
        ax.set_ylabel(r"sensitivity [$\mathrm{Hz}^{-1/2}$]")  # , fontsize=12)
        ax.grid("both", "both", color="gainsboro")
        ax.legend(
            ncol=1,
            handlelength=1,
            columnspacing=0.5,
            labelspacing=0.3,
            bbox_to_anchor=(0.3, 1),
            loc="upper left",
            frameon=True,
        )
        ax.add_patch(
            patches.Rectangle(
                (-0.37, -0.36),
                width=1.41,
                height=1.62,
                linewidth=1,
                edgecolor="gray",
                facecolor="none",
                transform=ax.transAxes,
                clip_on=False,
            )
        )
        ax.text(-0.32, 1.11, "(e)", transform=ax.transAxes, fontsize=22)

    fig.savefig(
        "./paper_figures_source/figures/submission/" + output_plot_label,
        bbox_inches="tight",
    )
