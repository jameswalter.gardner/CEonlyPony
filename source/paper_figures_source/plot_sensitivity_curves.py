"""Plot sensitivity curves for paper figure.

Usage:
    When in source/ directory for relative paths to be correct:
    $ python3 paper_figures_source/plot_sensitivity_curves.py

License:
    BSD 3-Clause License

    Copyright (c) 2022, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import numpy as np
import matplotlib.pyplot as plt

# change to LaTeX style fonts
import matplotlib

matplotlib.rcParams["mathtext.fontset"] = "stix"
matplotlib.rcParams["font.family"] = "STIXGeneral"

# include parent directory in import path
import sys, os

sys.path.insert(1, os.path.join(sys.path[0], ".."))
# import psd loader from parent directory and remember to update import path for data files below
from gwbench.psd import psd
from useful_plotting_functions import BANG_WONG_COLOURS

output_plot_label = "fig1.pdf"

# save_figure_path = "./paper_figures_source/figures/"
save_figure_path = "./paper_figures_source/figures/submission/"
data_files_path = "./paper_figures_source/data_files/sensitivity_curves_TXT_files"

style_dict = {
    #     "A+": {
    #         "label": "LIGO O5 (A+) reference",
    #         "color": "grey",
    #         "linestyle": "dotted",
    #         "linewidth": 1,
    #     },
    "A#-100Hzf-6": {
        # "label": r"A#-$f^{-6}$",
        # # A# with " + r"100 Hz $f^{-6}$ rolloff",
        "label": r"$\mathrm{A}\#_\mathrm{3Sus}$",
        # "color": "r",
        "color": BANG_WONG_COLOURS["Vermillion"],
        "linestyle": None,
        "linewidth": 4,
        "zorder": 3,
    },
    "A#": {
        "label": "A#",
        # "color": "tab:pink",
        "color": BANG_WONG_COLOURS["Orange"],
        "linestyle": (0, (3, 1)),
        "linewidth": 3,
    },
    "ET": {
        "label": r"$\mathrm{ET}_\mathrm{single}$",
        # "color": "saddlebrown",
        # borrowing from Category 20 
        "color": "#8C564B",
        "linestyle": (0, (1, 1, 3, 1)),
        "linewidth": 3,
    },
    "CE2-20-PMO": {
        # "label": "CE-20-PMO",
        # "label": "CE 20-km (PMO)",
        "label": "CE 20 km (PMO)",
        # "color": "g",
        "color": BANG_WONG_COLOURS["Bluish green"],
        "linestyle": None,
        "linewidth": 4,
        "zorder": 3,
    },
    "CE2-20-CBO": {
        # "label": "CE-20-CBO",
        # "label": "CE 20-km (CBO)",
        "label": "CE 20 km (CBO)",
        # "color": "m",
        "color": BANG_WONG_COLOURS["Reddish purple"],
        "linestyle": None,
        "linewidth": 4,
        "zorder": 3,
    },
    "CE2-40-CBO": {
        # "label": "CE-40-CBO",
        # "label": "CE 40-km (CBO)",
        "label": "CE 40 km (CBO)",
        # "color": "b",
        "color": BANG_WONG_COLOURS["Blue"],
        "linestyle": (0, (1, 1, 3, 1)),
        "linewidth": 3,
    },
    "V+": {
        # "label": "V+",
        "label": "Virgo+",
        # "color": "lightgreen",
        # borrowing from Category 20 
        "color": "#FF9896",
        "linestyle": (0, (3, 1)),
        "linewidth": 3,
    },
    "K+": {
        # "label": "K+",
        "label": "KAGRA+",
        # "color": "orange",
        "color": BANG_WONG_COLOURS["Yellow"],
        # "linestyle": "--",
        "linestyle": (0, (3, 1)),
        "linewidth": 3,
    },
    "Voyager-CBO": {
        # "label": "Voy",
        "label": "Voyager",
        # "color": "k",
        "color": BANG_WONG_COLOURS["Black"],
        # "linestyle": (0, (1, 1, 3, 1)),
        "linestyle": (0, (3, 1)),
        "linewidth": 3,
    },
}

conv_ET_to_Lshape = True
if conv_ET_to_Lshape:
    style_dict["ET"]["label"] = "ET"

yes_NEMO = False
if yes_NEMO:
    style_dict["NEMO"] = {
        "label": "NEMO",
        # "color": "c",
        "color": BANG_WONG_COLOURS["Sky blue"],
        "linestyle": None,
        "linewidth": 3,
    }

# plot sensitivity curve ASDs
plt.rcParams.update({"font.size": 22})
legend_pos = (-0.1, -0.17)
fig, ax = plt.subplots(figsize=(8, 6.5))

frequency_array = np.geomspace(5, 5000, 200)

flip_tuple_and_convert_to_asd = lambda tuple_psd_frequency: (
    tuple_psd_frequency[1],
    np.sqrt(tuple_psd_frequency[0]),
)
for tec, fmt_dict in style_dict.items():
    # using psd from gwbench.psd and updating to present data_files_path
    data = flip_tuple_and_convert_to_asd(
        psd(tec, frequency_array, path=data_files_path)
    )
    if tec == "ET" and conv_ET_to_Lshape:
        data = (data[0], 2 / 3 * data[1])
    ax.loglog(*data, **fmt_dict)

ax.set(
    ylim=(2e-25, 1e-23),
    xlim=(min(frequency_array), max(frequency_array)),
    xlabel="frequency [Hz]",
    ylabel=r"strain sensitivity $\left[\mathrm{Hz}^{-1/2}\right]$",
)
ax.set_xticklabels([0.1, 1, 10, 100, 1000])
ax.grid("both", "both", color="gainsboro")
ax.legend(
    ncol=3,
    handlelength=1,
    columnspacing=0.5,
    labelspacing=0.3,
    bbox_to_anchor=legend_pos,
    loc="upper left",
)
# highlight the audioband
# ax.axvspan(100, 1000, facecolor='0.7', alpha=0.5, zorder=-100)

fig.tight_layout()
# fig.savefig(save_figure_path + "sensitivity_curves.pdf", bbox_inches="tight")
fig.savefig(save_figure_path + output_plot_label, bbox_inches="tight")
plt.clf()
