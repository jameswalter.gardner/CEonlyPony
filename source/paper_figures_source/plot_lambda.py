"""Plot lambda statistic figures for paper.

Usage:
    When in source/ directory for relative paths to be correct:
    $ python3 paper_figures_source/plot_lambda.py

License:
    BSD 3-Clause License

    Copyright (c) 2023, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import matplotlib.pyplot as plt

# change to LaTeX style fonts
import matplotlib

matplotlib.rcParams["mathtext.fontset"] = "stix"
matplotlib.rcParams["font.family"] = "STIXGeneral"

# include parent directory in import path
import sys, os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

from filesystem_interaction import *
from useful_plotting_functions import BANG_WONG_COLOURS


def lambda_bound(australian_fractional_metric_value_GWTC: float, normalised: bool):
    # finding a lower bound on lambda
    # include a factor of 0.59 (updated to 0.68) as the resolution is set by the size of the unrejected population
    # this also means that a uniform rejection rate will not affect the lambda bound
    uniform_rejection_rate = 0.68
    observation_time_in_years = 10.0
    sources_in_range_GWTC2 = 133.9
    min_value_fraction = 1 / (
        uniform_rejection_rate * observation_time_in_years * sources_in_range_GWTC2
    )
    if normalised:
        fractional_metric = australian_fractional_metric_value_GWTC
    else:
        fractional_metric = (
            australian_fractional_metric_value_GWTC / sources_in_range_GWTC2
        )
    lambda_bound = fractional_metric / min_value_fraction
    return lambda_bound


def reorder_list(list_unordered: list, generation: str):
    if generation == "3G":
        reorder_AU_nets = (2, 1, 0, 3)
        reorder_baseline_nets = (1, 3, 0, 2)
    elif generation == "2HALFG":
        reorder_AU_nets = (2, 0, 1)
        reorder_baseline_nets = list(reversed(range(4)))
    else:
        raise ValueError()

    slices = [
        list_unordered[slice_index :: len(reorder_AU_nets)]
        for slice_index in reorder_AU_nets
    ]
    slices_reordered = [[slice[i] for i in reorder_baseline_nets] for slice in slices]
    list_reordered = [value for slice in slices_reordered for value in slice]
    return list_reordered


def net_label_to_paper_style(net_label: str):
    return (
        net_label.replace(", ", "\n")
        .replace("A#-$f^{-6}$", "$\mathrm{A}\#_\mathrm{3Sus}$ ")
        .replace("A#-100Hzf-6", "$\mathrm{A}\#_\mathrm{3Sus}$ ")
        .replace("Voy-CBO", "Voyager ")
        .replace("V+_V", "Virgo+")
        .replace("K+_K", "KAGRA+")
        .replace("ET_E", "ET")
        # .replace("CE-20-CBO_AU", "CE 20-km (CBO; AU)")
        # .replace("CE-20-PMO_AU", "CE 20-km (PMO; AU)")
        # .replace("CE-40-CBO", "CE 40-km ")
        # .replace("CE-20-CBO", "CE 20-km ")
        .replace("CE-20-CBO_AU", "CE 20 km (CBO; AU)")
        .replace("CE-20-PMO_AU", "CE 20 km (PMO; AU)")
        .replace("CE-40-CBO", "CE 40 km ")
        .replace("CE-20-CBO", "CE 20 km ")
        .replace("NEMO", "NEMO ")
        .replace("_H", "(H)")
        .replace("_L", "(L)")
        .replace("_I", "(I)")
        .replace("_C", "(C)")
        .replace("_N", "(N)")
        .replace("_AU", "(AU)")
    )


debug = False
generations = ["2HALFG", "3G"]
baseline_networks_dict = dict()
data_dir = "./paper_figures_source/data_files/summary_tables_H5_files/reliable/"
output_dir = "./paper_figures_source/data_files/lambda_statistic_H5_files/"

for generation in generations:
    # load metric values
    input_file = data_dir + f"{generation}.h5"
    summary_table = load_hdf(input_file)
    metric_dataframe = summary_table.loc[
        reorder_list(summary_table.index, generation=generation)
    ]

    metric_data = dict()
    metrics = ["SNR > 25, sky < 10", "SNR > 25, sky < 1"]
    for metric in metrics:
        # identify the baseline networks
        total_baseline_networks = 4
        total_AU_observatories = 3 if generation == "3G" else 2
        baseline_networks = metric_dataframe[:total_baseline_networks][metric]
        baseline_networks_dict[generation] = baseline_networks.index
        improved_networks = [
            metric_dataframe[
                total_baseline_networks * (i + 1) : total_baseline_networks * (i + 2)
            ][metric]
            for i in range(total_AU_observatories)
        ]

        # calculate lambda
        lambda_data = [dict() for _ in improved_networks]
        label_list_of_dicts = [dict() for _ in improved_networks]
        for index_AU_network, lambda_list in enumerate(lambda_data):
            label_dict = label_list_of_dicts[index_AU_network]
            improved_df = improved_networks[index_AU_network]
            if debug:
                print(index_AU_network, improved_df)

            for index_baseline_network in range(len(baseline_networks)):
                baseline = baseline_networks[index_baseline_network]
                australian = improved_df[index_baseline_network]
                improved_net_label = improved_df.index[index_baseline_network]
                if debug:
                    print(baseline, australian, improved_net_label)

                # handling dividing by zero
                if baseline == 0:
                    lambda_value = np.nan

                    lambda_bound_here = lambda_bound(australian, normalised=False)
                    label = lambda_bound_here

                    # label = r"$>$" + f"{lambda_bound_here:.0f}"
                else:
                    lambda_value = australian / baseline

                    label = np.nan
                    # if lambda_value > 10:
                    #     precision = 0
                    # else:
                    #     precision = 1
                    # label = f"{lambda_value:.{precision}f}"

                lambda_list[improved_net_label] = lambda_value
                label_dict[improved_net_label] = label

        # output lambda into dict
        flatten_lambda = {k: v for d in lambda_data for k, v in d.items()}
        flatten_labels = {k: v for d in label_list_of_dicts for k, v in d.items()}
        metric_dict = {
            metric: [*flatten_lambda.values()],
            f"{metric}: bound": [*flatten_labels.values()],
        }
        metric_data = dict(**metric_data, **metric_dict)

    network_AU_labels = [*flatten_lambda.keys()]
    lambda_flat_df = pd.DataFrame(data=metric_data, index=network_AU_labels)
    # display(lambda_flat_df)
    save_hdf(lambda_flat_df, output_dir + f"{generation}.h5")

for NEMO in [0, 1]:
    plt.rcParams.update({"font.size": 22})
    fig, axs = plt.subplots(
        nrows=2,
        ncols=2,
        # sharey="row",
        figsize=(9.5 * 2.2, 9.5),
        gridspec_kw={
            "height_ratios": [1, 1],
            "hspace": 0.67,
            "wspace": 0.25,
        },
    )

    for generation in generations:
        lambda_data = load_hdf(output_dir + f"{generation}.h5")
        for col in lambda_data.columns:
            plot_panel_1 = 0 if "2HALFG" == generation else 1
            plot_panel_2 = 0 if "10" in col else 1
            marker_style = "o" if "bound" not in col else "x"
            colours = (
                [
                    BANG_WONG_COLOURS["Bluish green"],
                    BANG_WONG_COLOURS["Reddish purple"],
                    BANG_WONG_COLOURS["Sky blue"],
                ]
                if "3G" == generation
                else [
                    BANG_WONG_COLOURS["Vermillion"],
                    BANG_WONG_COLOURS["Sky blue"],
                ]
            )

            for j, baseline_network in enumerate(baseline_networks_dict[generation]):
                net_label = net_label_to_paper_style(baseline_network)
                # hatting/grouping the data again
                # for k in range(3):
                data = lambda_data[col][j::4]
                for k, value in enumerate(data.values):
                    label0 = net_label_to_paper_style(
                        data.index[k].replace(baseline_network, "").replace(", ", "")
                    )
                    label = label0 if ("bound" not in col and j == 0) else ""
                    # label bound too if present
                    label = (
                        "bound" if ("bound" in col and any(~np.isnan(data))) else label
                    )
                    if not NEMO and "NEMO" in label0:
                        continue
                    elif NEMO and "NEMO" not in label0:
                        continue
                    axs[plot_panel_1, plot_panel_2].scatter(
                        net_label,
                        value,
                        zorder=2,
                        marker=marker_style,
                        s=50,
                        color=colours[k],
                        label=label,
                        # alpha=0.5,
                    )

    for ax in axs.flatten():
        ax.set_yscale("log")
        ax.grid(which="major", axis="y", color="gainsboro", zorder=0)
        ax.set_ylim(1, 200)
        ax.set_yticks([1, 10, 100])
        ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        # increase whitespace between xticklabel and xticks
        ax.tick_params(axis="x", which="major", pad=7)

    handles0, labels0 = axs[0, 0].get_legend_handles_labels()
    handles1, labels1 = axs[1, 0].get_legend_handles_labels()
    handles, labels = [*handles0, *handles1], [*labels0, *labels1]
    if NEMO:
        handles, labels = handles[:-1], labels[:-1]
    axs[0, 0].legend(
        handles,
        labels,
        ncol=1,
        labelspacing=0.1,
        handletextpad=0,
        frameon=True,
        fontsize=22,
        loc="upper right",
        # ncol=2 if any(['bound' in label for label in labels]) else 1,
        # columnspacing=0.1,
        # bbox_to_anchor=(0.3, 1),
        # handlelength=1,
        # loc="lower left" if 'CE' in labels[0] else 'upper right',
    )

    # ylabel = r'factor of improvement in metric, $\lambda$'
    ylabel = r"geometric improvement in sources localised within"
    for col_index in range(2):
        # ylabel0 = ylabel.replace("metric", ["loose", "tight"][col_index] + " metric")
        ylabel0 = ylabel + [r"$10\,\mathrm{deg}^2$", r"$1\,\mathrm{deg}^2$"][col_index]
        axs[1, col_index].text(
            -0.1,
            0,
            ylabel0,
            transform=axs[1, col_index].transAxes,
            rotation=90,
            rotation_mode="anchor",
        )

    ypos = 1.03
    axs[0, 0].text(0.01, ypos, "(a)", transform=axs[0, 0].transAxes)
    axs[0, 1].text(0.01, ypos, "(b)", transform=axs[0, 1].transAxes)
    axs[1, 0].text(0.01, ypos, "(c)", transform=axs[1, 0].transAxes)
    axs[1, 1].text(0.01, ypos, "(d)", transform=axs[1, 1].transAxes)

    fig.savefig(
        "./paper_figures_source/figures/submission/"
        + f'fig{"3" if not NEMO else "5"}.pdf',
        bbox_inches="tight",
    )
    plt.clf()
    # plt.show()
