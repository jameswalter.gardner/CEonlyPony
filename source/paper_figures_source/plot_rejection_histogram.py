"""Plot rejection profile for paper figure.

TODO: address the following warning
paper_figures_source/plot_rejection_histograms.py:166: UserWarning: This figure includes Axes that are not compatible with tight_layout, so results might be incorrect.
  fig.tight_layout()

Usage:
    When in source/ directory for relative paths to be correct:
    $ python3 paper_figures_source/plot_rejection_histogram.py

License:
    BSD 3-Clause License

    Copyright (c) 2023, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob
import pickle

# change to LaTeX style fonts
import matplotlib

matplotlib.rcParams["mathtext.fontset"] = "stix"
matplotlib.rcParams["font.family"] = "STIXGeneral"

# include parent directory in import path
import sys, os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

from filesystem_interaction import load_hdf
from constants import EM_FOLLOWUP_REDSHIFT_HI
from useful_plotting_functions import BANG_WONG_COLOURS

# - - - user inputs - - -
output_plot_label = "fig7.pdf"

plot_unified_inclination = True

plot_individual_inclination = False
# plot_individual_inclination = True

plot_combined_inclination = False
# plot_combined_inclination = True

plot_redshift = False
# plot_redshift = True

plot_chirp_mass = False
# plot_chirp_mass = True
# - - - - - -_

no_data = False

if plot_unified_inclination:
    # indicies in unification
    with open("./reunification/unified_indices/hat_indices.pkl", "rb") as input_file:
        hat_indices = pickle.load(input_file)
    indicies_allowed_multiple_detector_unification = hat_indices["ET_E..CE-40-CBO_C"][
        "unified_with_all_multiple_detector_runs"
    ]["indicies_allowed"]["multiple_detector"]

    # raw data to profile inclination angles of rejections
    # TODO: upload data file as a release to gitlab since I cannot add it directly to the commit since it is too large
    if not no_data:
        raw_data = dict()
        df_raw = load_hdf(
            "reunification/extracted_data/31390222/data_raw_injections/SCI-CASE_BNS_INJS-PER-ZBIN_250000.h5"
        )
        # in range data
        raw_data["total"] = df_raw[df_raw['z'] <= EM_FOLLOWUP_REDSHIFT_HI]
        raw_data["rejections"] = raw_data["total"][
            ~raw_data["total"]["injection_index"].isin(
                indicies_allowed_multiple_detector_unification
            )
        ]
        raw_data["successes"] = raw_data["total"][
            raw_data["total"]["injection_index"].isin(
                indicies_allowed_multiple_detector_unification
            )
        ]

        # sanity checks
        if not (
            len(raw_data["successes"]) + len(raw_data["rejections"])
            == len(raw_data["total"])
            and f"{len(raw_data['rejections'])/len(raw_data['total']):.0%}" == "68%"
        ):
            raise ValueError("Rejections missing.")

    # plot rejection histogram for unified population
    plt.rcParams.update({"font.size": 22})
    fig, ax = plt.subplots(figsize=(8, 4))
    if not no_data:
        ax.hist(
            (
                np.cos(raw_data["rejections"]["iota"]),
                np.cos(raw_data["successes"]["iota"]),
                np.cos(raw_data["total"]["iota"]),
            ),
            density=False,
            # color=["darkorange", "lightgreen", "dodgerblue"],
            color=[BANG_WONG_COLOURS["Orange"], BANG_WONG_COLOURS["Yellow"], BANG_WONG_COLOURS["Blue"]],
            edgecolor="k",
            label=["rejected", "unrejected", "total"],
            zorder=3,
        )
    else:
        ax.hist(
            (
                np.cos(np.random.randint(5e4, 10e4, 5)),
                np.cos(np.random.randint(5e4, 10e4, 5)),
                np.cos(np.random.randint(5e4, 10e4, 5)),
            ),
            density=False,
            color=["darkorange", "lightgreen", "dodgerblue"],
            edgecolor="k",
            label=["rejected", "unrejected", "total"],
            zorder=3,
        )
    ax.grid("both", "both", color="gainsboro", zorder=0)
    ax.set_ylabel(r"count in bin [$10^3$]")
    ax.set_xlabel(
        "cosine of inclination angle between\nthe line-of-sight and the orbital plane"
    )
    ax.set_xlim((-1, 1))
    ax.set_xticks([-1, -0.5, 0, 0.5, 1], labels=['-1', '-0.5', '0', '0.5', '1'])
    ax.set_yticks([0, 2e3, 4e3], labels=['0', '2', '4'])
    ax.legend(
        ncol=3,
        columnspacing=0.5,
        labelspacing=0.3,
        bbox_to_anchor=(0.01, -0.32),
        loc="upper left",
    )

    path = "./paper_figures_source/figures/submission/"
    if not os.path.exists(path):
        os.makedirs(path)
    fig.savefig(path + output_plot_label, bbox_inches="tight")
    plt.close()

# preserving the code for other histograms mentioned in the text but not shown in the paper
plot_other_than_unified = (
    plot_individual_inclination
    or plot_combined_inclination
    or plot_redshift
    or plot_chirp_mass
)
if plot_other_than_unified:
    files = sorted(
        glob.glob(
            "reunification/extracted_data/*/data_processed_injections/*.h5",
            recursive=True,
        )
    )
    # remove the sharp frequency cuts since they aren't used in the plots
    # 'Hz_' is in, e.g., A+-100Hz_AU where as the roll-offs are 'Hzf-', e.g. A#-100Hzf-6_AU
    files = [file for file in files if "Hz_" not in file]

    # print(f"{len(files)} files")
    # for file in files: print(file.split('/')[-1].split('NET_')[1].split('_SCI-CASE')[0].split('..'))

    file_tag_dict = {
        f"{os.path.basename(file).split('NET_')[1].split('_SCI-CASE')[0].split('..')}: {file.split('/')[2]}": file
        for file in files
    }

    raw_data = load_hdf(
        "reunification/extracted_data/31390222/data_raw_injections/SCI-CASE_BNS_INJS-PER-ZBIN_250000.h5"
    )
    rejection_rate_df = load_hdf("reunification/rejection_rate/rejection_rate.h5")
    file_to_rejection_rate_dict = dict()
    for _, row in rejection_rate_df.iterrows():
        file_tag = f"{row['network_spec']}: {row['run_ID']}"
        file = file_tag_dict[file_tag]
        file_to_rejection_rate_dict[file] = row["rejection_rate"]

    files_to_profile = [
        # the lowest rejection rate network, 10% rejections
        "reunification/extracted_data/20220825/data_processed_injections/NET_ET_E..CE-20-PMO_AU_SCI-CASE_BNS_WF_lal_bns_IMRPhenomD_NRTidalv2_INJS-PER-ZBIN_250000.h5",
        # the highest rejection rate multiple-detector network, 33% rejections
        "reunification/extracted_data/30830111/data_processed_injections/NET_NEMO_AU..CE-40-CBO_C_SCI-CASE_BNS_WF_lal_bns_IMRPhenomD_NRTidalv2_INJS-PER-ZBIN_250000.h5",
        # one of the two single-detector network runs, 94% rejections
        "reunification/extracted_data/20220825/data_processed_injections/NET_CE-40-CBO_C_SCI-CASE_BNS_WF_lal_bns_IMRPhenomD_NRTidalv2_INJS-PER-ZBIN_250000.h5",
    ]

    rejection_data_dict = dict()
    for file in files_to_profile:
        rejection_data_dict[file] = dict()

        rejection_data_dict[file]["processed_data"] = load_hdf(file)

        rejection_data_dict[file]["unrejected_processed_data"] = rejection_data_dict[
            file
        ]["processed_data"].loc[
            rejection_data_dict[file]["processed_data"]["rejection_flag"]
            == "not_rejected"
        ]
        rejection_data_dict[file]["unrejected_raw_data"] = raw_data.loc[
            raw_data["injection_index"].isin(
                rejection_data_dict[file]["unrejected_processed_data"][
                    "injection_index"
                ]
            )
        ]

        rejection_data_dict[file]["rejections_processed_data"] = rejection_data_dict[
            file
        ]["processed_data"].loc[
            rejection_data_dict[file]["processed_data"]["rejection_flag"]
            == "rejected_for_ill-conditioned_FIM"
        ]
        rejection_data_dict[file]["rejections_raw_data"] = raw_data.loc[
            raw_data["injection_index"].isin(
                rejection_data_dict[file]["rejections_processed_data"][
                    "injection_index"
                ]
            )
        ]

    # - - - Histogram against inclination angle - - -
    # for the later runs, these histograms will have a discrepancy in the total given by the 864 missing injections, but they are only 0.06% and will not appear visually, TODO: perhaps profile the missing injections given infinite time
    if plot_individual_inclination:
        for file in files_to_profile:
            network_label = os.path.basename(file).split("_SCI-CASE")[0]

            plt.rcParams.update({"font.size": 18})
            fig, axs = plt.subplots(
                nrows=1,
                ncols=2,
                sharey=True,
                figsize=(8, 3),
                gridspec_kw=dict(wspace=0.05),
            )

            axs[0].hist(
                (
                    rejection_data_dict[file]["rejections_raw_data"]["iota"],
                    rejection_data_dict[file]["unrejected_raw_data"]["iota"],
                    raw_data["iota"],
                    # = np.concatenate((unrejected_raw_data['iota'], rejections_raw_data['iota'])),
                ),
                #     density=True,
                density=False,
                label=["rejected", "unrejected", "total"],
                color=["r", "g", "b"],
            )
            axs[0].legend(
                ncol=3,
                #     columnspacing=0.5,
                bbox_to_anchor=(-0.25, -0.3),
                loc="upper left",
            )
            # axs[0].set_ylabel('count in bin / (total number $\times$ bin width)')
            axs[0].set_ylabel("count in bin")
            axs[0].set_xlabel("inclination, $\iota$")
            labels = [
                "$0$",
                r"$\frac{\pi}{4}$",
                r"$\frac{\pi}{2}$",
                r"$\frac{3\pi}{4}$",
                r"$\pi$",
            ]
            axs[0].set_xticks(np.arange(0, np.pi + 0.01, np.pi / 4))
            axs[0].set_xticklabels(labels)
            axs[0].grid("both", "both", color="gainsboro")
            fig.suptitle(
                f"histogram of the inclination angle between\nthe line-of-sight and the orbital plane\nin the non-cosmological population for\n{network_label} with {file_to_rejection_rate_dict[file]:.0%} rejections",
                y=1.35,
            )

            # gwbench's injections.py samples iota and dec uniformly under cosine, the other two angles are uniform themselves
            axs[1].hist(
                (
                    np.cos(rejection_data_dict[file]["rejections_raw_data"]["iota"]),
                    np.cos(rejection_data_dict[file]["unrejected_raw_data"]["iota"]),
                    np.cos(raw_data["iota"]),
                ),
                #     density=True,
                density=False,
                color=["r", "g", "b"],
            )
            axs[1].set_xlabel("cosine of inclination, $\cos(\iota)$")
            axs[1].grid("both", "both", color="gainsboro")

            fig.align_labels()
            #     fig.tight_layout()
            path = "./paper_figures_source/figures/rejections/inclination/"
            if not os.path.exists(path):
                os.makedirs(path)
            fig.savefig(path + f"{network_label}.pdf", bbox_inches="tight")

            plt.close()

    # multi network plot
    files_to_profile_for_combined_plot = files_to_profile[:-1]

    if plot_combined_inclination:
        plt.rcParams.update({"font.size": 22})
        fig, axs = plt.subplots(
            nrows=2,
            ncols=1,
            sharex=True,
            sharey=True,
            figsize=(8, 4),
            #         gridspec_kw=dict(hspace=0.36),
            gridspec_kw=dict(hspace=0.15),
        )

        for i, file in enumerate(files_to_profile_for_combined_plot):
            network_label = (
                os.path.basename(file)
                .split("_SCI-CASE")[0]
                .replace("NET_", "")
                .replace("..", ", ")
            )  #',\n')
            rejection_rate = file_to_rejection_rate_dict[file]
            axs[i].hist(
                (
                    np.cos(rejection_data_dict[file]["rejections_raw_data"]["iota"]),
                    np.cos(rejection_data_dict[file]["unrejected_raw_data"]["iota"]),
                    np.cos(raw_data["iota"]),
                ),
                #     density=True,
                density=False,
                #             color=["r", "g", "b"],
                color=["darkorange", "lightgreen", "dodgerblue"],
                edgecolor="k",
                label=["rejected", "unrejected", "total"],
                zorder=3,
            )
            axs[i].grid("both", "both", color="gainsboro", zorder=0)
            #         axs[i].set_ylabel(r"count in bin [$10^4$]")
            #         axs[i].set_title(f"{network_label}\n({rejection_rate:.0%} rejected)", fontsize=18)
            axs[i].set_yticks(range(0, 200000, 50000))
            axs[i].set_yticklabels([0, 5, 10, 15])

        axs[-1].legend(
            ncol=2,
            columnspacing=0.5,
            labelspacing=0.3,
            bbox_to_anchor=(-0.05, -0.7),
            loc="upper left",
        )
        axs[-1].set_xlabel(
            "cosine of inclination angle between\nthe line-of-sight and the orbital plane"
        )

        #     fig.suptitle(
        #         f"histogram of the inclination angle between\nthe line-of-sight and the orbital plane\nin the non-cosmological population",
        #         y=1.05,
        #     )
        fig.align_labels()

        # shared ylabel
        fig.add_subplot(111, frameon=False)
        # hide tick and tick label of the big axis
        plt.tick_params(
            labelcolor="none",
            which="both",
            top=False,
            bottom=False,
            left=False,
            right=False,
        )
        plt.ylabel(r"count in bin [$10^4$]", labelpad=5)

        #     path = "./paper_figures_source/figures/rejections/inclination/"
        path = "./paper_figures_source/figures/submission/"
        if not os.path.exists(path):
            os.makedirs(path)
        #     fig.savefig(path + f"combined_plot.pdf", bbox_inches="tight")
        fig.savefig(path + "fig3.pdf", bbox_inches="tight")
        plt.close()

    # - - - Histogram against redshift - - -
    if plot_redshift:
        for file in files_to_profile:
            network_label = os.path.basename(file).split("_SCI-CASE")[0]

            fig, axs = plt.subplots(
                nrows=1,
                ncols=2,
                sharey=False,
                figsize=(8, 3),
                gridspec_kw=dict(wspace=0.4),
            )
            #     axs = (ax, )

            axs[0].hist(
                (
                    rejection_data_dict[file]["rejections_raw_data"]["z"],
                    rejection_data_dict[file]["unrejected_raw_data"]["z"],
                    raw_data["z"],
                    #             np.log(rejection_data_dict[file]["rejections_raw_data"]["z"]),
                    #             np.log(rejection_data_dict[file]["unrejected_raw_data"]["z"]),
                    #             np.log(raw_data["z"]),
                ),
                bins=[0, 0.5, 1, 2, 4, 10, 50],
                #         bins=np.geomspace(0.02, 50, 10),
                density=False,
                label=["rejected", "unrejected", "total"],
                color=["r", "g", "b"],
                zorder=3,
            )
            axs[0].axvline(
                EM_FOLLOWUP_REDSHIFT_HI, color="grey", linestyle="--", label="threshold"
            )
            axs[0].legend(
                ncol=2,
                bbox_to_anchor=(-0.25, -0.3),
                loc="upper left",
            )
            axs[0].set_ylabel("count in bin")
            axs[0].set_xlabel("logarithm of redshift, $\log(z)$")
            axs[0].set_xscale("log")
            axs[0].grid("both", "both", color="gainsboro", zorder=0)

            axs[1].hist(
                (
                    rejection_data_dict[file]["rejections_raw_data"]["z"],
                    rejection_data_dict[file]["unrejected_raw_data"]["z"],
                    raw_data["z"],
                    #             np.log(rejection_data_dict[file]["rejections_raw_data"]["z"]),
                    #             np.log(rejection_data_dict[file]["unrejected_raw_data"]["z"]),
                    #             np.log(raw_data["z"]),
                ),
                bins=np.geomspace(0.02, EM_FOLLOWUP_REDSHIFT_HI * 2, 10),
                density=False,
                label=["rejected", "unrejected", "total"],
                color=["r", "g", "b"],
                zorder=3,
            )
            axs[1].axvline(
                EM_FOLLOWUP_REDSHIFT_HI, color="grey", linestyle="--", label="threshold"
            )

            #     axs[1].set_ylabel("count in bin")
            axs[1].set_xlabel("logarithm of redshift, $\log(z)$")
            axs[1].set_xlim((0.02, EM_FOLLOWUP_REDSHIFT_HI * 2))
            axs[1].set_xscale("log")
            axs[1].grid("both", "both", color="gainsboro", zorder=0)

            fig.suptitle(
                f"histogram of the redshift in the\nnon-cosmological population for\n{network_label} with {file_to_rejection_rate_dict[file]:.0%} rejections",
                y=1.3,
            )

            fig.align_labels()
            fig.tight_layout()
            path = "./paper_figures_source/figures/rejections/redshift/"
            if not os.path.exists(path):
                os.makedirs(path)
            fig.savefig(path + f"{network_label}.pdf", bbox_inches="tight")
            plt.close()

    # - - - Histogram against chirp mass (dist. Gaussian in uniform population) - - -
    if plot_chirp_mass:
        for file in files_to_profile:
            network_label = os.path.basename(file).split("_SCI-CASE")[0]

            fig, axs = plt.subplots(
                nrows=1,
                ncols=3,
                sharey=False,
                figsize=(12, 3),
                gridspec_kw=dict(wspace=0.33),
            )
            #     axs = (ax, )

            axs[0].hist(
                (
                    rejection_data_dict[file]["rejections_raw_data"]["Mc"],
                    rejection_data_dict[file]["unrejected_raw_data"]["Mc"],
                    raw_data["Mc"],
                ),
                density=False,
                label=["rejected", "unrejected", "total"],
                color=["r", "g", "b"],
                zorder=3,
            )
            axs[0].legend(
                ncol=3,
                bbox_to_anchor=(0.45, -0.4),
                loc="upper left",
            )
            axs[0].set_ylabel("count in bin")
            axs[0].set_xlabel(
                "observed chirp mass,\n$(1+z)(M_c)_\mathrm{source}$ [$M_\odot$]"
            )
            axs[0].grid("both", "both", color="gainsboro", zorder=0)
            fig.suptitle(
                f"histogram of the chirp mass\nin the non-cosmological population for\n{network_label} with {file_to_rejection_rate_dict[file]:.0%} rejections",
                y=1.3,
            )

            axs[1].hist(
                (
                    rejection_data_dict[file]["rejections_raw_data"]["Mc"]
                    / (1 + rejection_data_dict[file]["rejections_raw_data"]["z"]),
                    rejection_data_dict[file]["unrejected_raw_data"]["Mc"]
                    / (1 + rejection_data_dict[file]["unrejected_raw_data"]["z"]),
                    raw_data["Mc"] / (1 + raw_data["z"]),
                ),
                density=False,
                label=["rejected", "unrejected", "total"],
                color=["r", "g", "b"],
                zorder=3,
            )
            axs[1].set_xlabel("source chirp mass,\n$(M_c)_\mathrm{source}$ [$M_\odot$]")
            axs[1].set_ylabel("count in bin / 100000")
            axs[1].set_yticks(range(0, 500000, 100000))
            axs[1].set_yticklabels(range(0, 5, 1))
            axs[1].grid("both", "both", color="gainsboro", zorder=0)

            axs[2].hist(
                (
                    rejection_data_dict[file]["rejections_raw_data"]["Mc"]
                    / (1 + rejection_data_dict[file]["rejections_raw_data"]["z"]),
                    rejection_data_dict[file]["unrejected_raw_data"]["Mc"]
                    / (1 + rejection_data_dict[file]["unrejected_raw_data"]["z"]),
                    raw_data["Mc"] / (1 + raw_data["z"]),
                ),
                density=True,
                label=["rejected", "unrejected", "total"],
                color=["r", "g", "b"],
                zorder=3,
            )
            axs[2].set_xlabel("source chirp mass,\n$(M_c)_\mathrm{source}$ [$M_\odot$]")
            axs[2].set_ylabel(
                "$\dfrac{\mathrm{count\; in\; bin}}{\mathrm{total\; count}\cdot\mathrm{bin\; width}}$"
            )
            axs[2].grid("both", "both", color="gainsboro", zorder=0)

            fig.align_labels()
            fig.tight_layout()
            path = "./paper_figures_source/figures/rejections/chirp_mass/"
            if not os.path.exists(path):
                os.makedirs(path)
            fig.savefig(path + f"{network_label}.pdf", bbox_inches="tight")

            plt.close()
