"""Plot separate (loose and tight) but combined generations metric hat graphs for paper.

Usage:
    When in source/ directory for relative paths to be correct:
    $ python3 paper_figures_source/plot_separate_metric_hat_graphs.py

License:
    BSD 3-Clause License

    Copyright (c) 2023, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from typing import List, Set, Dict, Tuple, Optional, Union, Callable
from numpy.typing import NDArray

import numpy as np
import matplotlib.pyplot as plt

# include parent directory in import path
import sys, os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

from useful_plotting_functions import plot_hat_graph
from summary_table import summary_table

tables_path = "./paper_figures_source/data_files/summary_tables_H5_files/reliable/"
legend_kwargs = dict()

subset_plot_options = dict(
    plot_fraction_in_range=True,
    display_precision=0,
    use_percentage=True,
)
unification_level = "unified_with_all_multiple_detector_runs"
input_dir = tables_path

metric_settings_dict = dict(
    loose=dict(
        reliable_metric = "SNR > 25, sky < 10",
        truncated_ylims = (0.3, 1,),
        ylabel = r"fractional $\mathbf{loose}$ follow-up metric",
        hspace = 0.6,
    ),
    tight=dict(
        reliable_metric = "SNR > 25, sky < 1",
        truncated_ylims = (0.2, 0.9),
        ylabel = r"fractional $\mathbf{tight}$ follow-up metric",
        hspace = 0.73,
    ),
)

# what AU networks to show
settings_networks_shown = dict(
    main_text = dict(
        no_reference_network=False,
        non_NEMO=True,
        NEMO=False,
    ),
    appendix_nemo = dict(
        no_reference_network=False,
        non_NEMO=False,
        NEMO=True,
    ),
)

user_selection_of_which_plots_dict = dict(
    fig2 = dict(output_fig_label = "fig2.pdf", **settings_networks_shown["main_text"], **metric_settings_dict["loose"]),
    fig3 = dict(output_fig_label = "fig3.pdf", **settings_networks_shown["main_text"], **metric_settings_dict["tight"]),
    fig6 = dict(output_fig_label = "fig6.pdf", **settings_networks_shown["appendix_nemo"], **metric_settings_dict["loose"]),
    fig7 = dict(output_fig_label = "fig7.pdf", **settings_networks_shown["appendix_nemo"], **metric_settings_dict["tight"]),
)

for _, settings_dict in user_selection_of_which_plots_dict.items():
    # TODO: figure out a more efficient way to unpack the settings
    reliable_metric = settings_dict["reliable_metric"]
    truncated_ylim1, truncated_ylim2 = settings_dict["truncated_ylims"]
    ylabel = settings_dict["ylabel"]
    output_fig_label = settings_dict["output_fig_label"]
    hspace = settings_dict["hspace"]
    no_reference_network = settings_dict["no_reference_network"]
    non_NEMO = settings_dict["non_NEMO"]
    NEMO = settings_dict["NEMO"]

    # general kwargs for plotting each generation
    generation_plot_kwargs_dict = {
        "2HALFG": dict(
            kwargs_for_loading_table=dict(
                metric=reliable_metric,
                reorder=(3, 2, 1, 0),
                network_labels_plot=[
                    "Voy_H,\nVoy_L,\nV+_V,\nK+_K,\nVoy_I",
                    "Voy_H,\nVoy_L,\nV+_V,\nK+_K",
                    "Voy_H,\nVoy_L,\nV+_V",
                    "Voy_H,\nVoy_L",
                ],
                plot_file_tag="2HALFG",
                hat_data_indices=[
                    (2, None, 3),
                    # (0, None, 3),
                    # (1, None, 3)
                ],
            ),
            kwargs_for_save_hat_graph=dict(
                australian_labels=[
                    "no AU detector",
                    # r"A#-$f^{-6}$_AU",
                    # "NEMO_AU",
                ],
                colour_list=[
                    None,
                    # "r",
                    # "c"
                ],
                legend_kwargs=legend_kwargs,
                annotation_fontsize=16,
                abbreviate_lambda_label=True,
            ),
        ),
        "3G": dict(
            kwargs_for_loading_table=dict(
                metric=reliable_metric,
                reorder=(1, 3, 0, 2),
                network_labels_plot=[
                    "CE-40-CBO_C,\nCE-20-CBO_N",
                    "CE-40-CBO_C",
                    "ET_E,\nCE-40-CBO_C",
                    "ET_E",
                ],
                plot_file_tag="3G",
                plot_file_tag_submission="fig4",
                hat_data_indices=[
                    (2, None, 4),
                    # (3, None, 4),
                    # (1, None, 4),
                    # (0, None, 4),
                ],
            ),
            kwargs_for_save_hat_graph=dict(
                australian_labels=[
                    "no AU detector",  # (PMO run)',
                    #     'with no Aus. detector (CBO run)',
                    # "NEMO_AU",
                    # "CE-20-PMO_AU",
                    # "CE-20-CBO_AU",
                ],
                colour_list=[
                    None,
                    # "c",
                    # "g",
                    # "m"
                ],
                legend_kwargs=dict(fontsize=19),
                annotation_fontsize=12,
                abbreviate_lambda_label=True,
            ),
        ),
    }

    # no_reference_network existing detectors if only going to show NEMO
    if no_reference_network:
        for _, generation_plot_kwargs in generation_plot_kwargs_dict.items():
            generation_plot_kwargs["kwargs_for_save_hat_graph"]["australian_labels"] = []
            generation_plot_kwargs["kwargs_for_save_hat_graph"]["colour_list"] = []
            generation_plot_kwargs["kwargs_for_loading_table"]["hat_data_indices"] = []

    # add A# and CEs to plot settings
    if non_NEMO:
        generation_plot_kwargs_dict["2HALFG"]["kwargs_for_save_hat_graph"]["australian_labels"].append(r"A#-$f^{-6}$_AU")
        generation_plot_kwargs_dict["2HALFG"]["kwargs_for_save_hat_graph"]["colour_list"].append("r")
        generation_plot_kwargs_dict["2HALFG"]["kwargs_for_loading_table"]["hat_data_indices"].append((0, None, 3))
        
        generation_plot_kwargs_dict["3G"]["kwargs_for_save_hat_graph"]["australian_labels"] += ["CE-20-PMO_AU", "CE-20-CBO_AU"]
        generation_plot_kwargs_dict["3G"]["kwargs_for_save_hat_graph"]["colour_list"] += ["g", "m"]
        generation_plot_kwargs_dict["3G"]["kwargs_for_loading_table"]["hat_data_indices"]  += [(1, None, 4), (0, None, 4)]

    # add NEMO to plot settings
    if NEMO:
        generation_plot_kwargs_dict["2HALFG"]["kwargs_for_save_hat_graph"]["australian_labels"].append("NEMO_AU")
        generation_plot_kwargs_dict["2HALFG"]["kwargs_for_save_hat_graph"]["colour_list"].append("c")
        generation_plot_kwargs_dict["2HALFG"]["kwargs_for_loading_table"]["hat_data_indices"].append((1, None, 3))
        
        generation_plot_kwargs_dict["3G"]["kwargs_for_save_hat_graph"]["australian_labels"].insert(1, "NEMO_AU")
        generation_plot_kwargs_dict["3G"]["kwargs_for_save_hat_graph"]["colour_list"].insert(1, "c")
        generation_plot_kwargs_dict["3G"]["kwargs_for_loading_table"]["hat_data_indices"].insert(1, (3, None, 4))

    plt.rcParams.update({"font.size": 22})
    fig, axs = plt.subplots(
        nrows=2,
        ncols=1,
        figsize=(9.5, 9.5 * (truncated_ylim1 + truncated_ylim2)/(0.3 + 1)),
        gridspec_kw={
            "height_ratios": [truncated_ylim1, truncated_ylim2],
            "hspace": hspace,
        },
    )

    for i, (
        generation,
        generation_plot_kwargs,
    ) in enumerate(generation_plot_kwargs_dict.items()):
        input_path = f"{input_dir}{generation}.h5"

        source_counts_in_distance = {"in max range": 133.9}
        table = summary_table(
            save_path=input_path,
            return_table=True,
            display_table=False,
            save_html=False,
            reliability_thresholds=True,
        )

        metric = generation_plot_kwargs["kwargs_for_loading_table"]["metric"]
        # split into different Australian detectors
        hat_data = np.array(
            [
                table[metric][i0:i1:i2]
                for (i0, i1, i2) in generation_plot_kwargs["kwargs_for_loading_table"][
                    "hat_data_indices"
                ]
            ]
        )
        # re-order networks for pleasant plotting
        reorder = generation_plot_kwargs["kwargs_for_loading_table"]["reorder"]
        reorder_nets_for_plot = lambda itr: [itr[i] for i in reorder]
        hat_data = hat_data[:, reorder]

        plot_kwargs = dict(
            network_labels_plot=reorder_nets_for_plot(
                generation_plot_kwargs["kwargs_for_loading_table"][
                    "network_labels_plot"
                ]
            ),
            # reordered above
            hat_data=hat_data,
            source_counts_in_distance=source_counts_in_distance,
            #             output_path=output_path,
            **generation_plot_kwargs["kwargs_for_save_hat_graph"],
            display_precision=subset_plot_options["display_precision"],
            use_percentage=subset_plot_options["use_percentage"],
            plot_fraction_in_range=subset_plot_options["plot_fraction_in_range"],
        )

        #         output_path = output_path.replace(".pdf", "_fractional.pdf")
        hat_data /= source_counts_in_distance["in max range"]
        max_range = 1

        # TODO: functionalise the lambda calculation and bound
        # calculate the lambda values for each Australian detector hat
        # lists for baseline network_labels along x-axis
        debug = False
        abbreviate_lambda_label = generation_plot_kwargs["kwargs_for_save_hat_graph"][
            "abbreviate_lambda_label"
        ]

        if debug:
            print(hat_data)
        baseline_data_list = hat_data[0]
        annotation_array = np.empty_like(hat_data, dtype=np.dtype(object))
        annotation_array[0] = ["" for _ in baseline_data_list]

        for j, australian_data_list in enumerate(hat_data[1:]):
            # additive change
            delta_list = australian_data_list - baseline_data_list
            # multiplicative change
            lambda_list = np.empty_like(baseline_data_list)
            label_list = np.empty_like(baseline_data_list, dtype=np.dtype(object))
            for k, baseline in enumerate(baseline_data_list):
                # handling dividing by zero
                if baseline == 0:
                    lambda_list[k] = np.nan
                    # finding a lower bound on lambda
                    observation_time_in_years = 10.0
                    # include a factor of 0.59 as the resolution is set by the size of the unrejected population
                    # this also means that a uniform rejection rate will not affect the lambda bound
                    uniform_rejection_rate = 0.59
                    min_value_fraction = 1 / (
                        uniform_rejection_rate
                        * observation_time_in_years
                        * source_counts_in_distance["in max range"]
                    )
                    lambda_bound = australian_data_list[k] / min_value_fraction
                    # TODO: import number of sources in range from somewhere
                    label_list[k] = r"$\lambda>$" + f"{lambda_bound:.0f}"
                else:
                    lambda_list[k] = australian_data_list[k] / baseline
                    if lambda_list[k] > 10:
                        precision = 0
                    else:
                        precision = 1
                    label_list[k] = r"$\lambda=$" + f"{lambda_list[k]:.{precision}f}"
                if abbreviate_lambda_label:
                    if debug:
                        print(label_list[k])
                    #                     label_list[k] = label_list[k][10:]
                    label_list[k] = (
                        label_list[k]
                        .replace("\\lambda", "")
                        .replace("=", "")
                        .replace("$", "")
                    )
            if debug:
                print(label_list)
            annotation_array[j + 1] = label_list
        if debug:
            print(annotation_array, "\n")

        # hack for submission
        add_asterix_for_single_detector_network = True if i == 1 else False

        # plot hat graph on ax
        ax = axs[i]
        plot_hat_graph(
            ax,
            reorder_nets_for_plot(
                generation_plot_kwargs["kwargs_for_loading_table"][
                    "network_labels_plot"
                ]
            ),
            # already reordered above
            hat_data,
            group_labels=generation_plot_kwargs["kwargs_for_save_hat_graph"][
                "australian_labels"
            ],
            **generation_plot_kwargs["kwargs_for_save_hat_graph"],
            annotations_off=False,
            spacing_between_hats=0.2,
            annotation_array=annotation_array,
            add_asterix_for_single_detector_network=add_asterix_for_single_detector_network,
        )

        truncated_ylim = truncated_ylim1 if i==0 else truncated_ylim2
        ax.set_ylim(0, truncated_ylim * max_range)
        major_ticks = np.arange(0, 1 + 0.1, 0.2)
        ax.yaxis.set_ticks(major_ticks, minor=False)
        ax.yaxis.set_ticklabels(
            ["{:.0%}".format(tick) for tick in major_ticks], minor=False
        )
        minor_ticks = major_ticks[:-1] + 0.1
        ax.yaxis.set_ticks(minor_ticks, minor=True)
        ax.set_ylim(0, truncated_ylim * max_range)

        ax.grid(which="both", axis="y", color="gainsboro", zorder=0)

    handles0, labels0 = axs[0].get_legend_handles_labels()
    handles, labels = axs[-1].get_legend_handles_labels()
    if len(handles0) > 1:
        if handles0[1] not in handles:
            handles.insert(1, handles0[1])
        if labels0[1] not in labels:
            labels.insert(1, labels0[1])
    axs[-1].legend(handles, labels, fontsize=16, loc="upper left")   

    # add ylabel to big axis
    fig.add_subplot(111, frameon=False)
    # hide tick and tick label of the big axis
    plt.tick_params(
        labelcolor="none", which="both", top=False, bottom=False, left=False, right=False
    )
    plt.ylabel(ylabel, labelpad=30)

    fig.savefig(
        "./paper_figures_source/figures/submission/" + output_fig_label,
        bbox_inches="tight",
    )
