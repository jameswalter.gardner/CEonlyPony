"""Plot rejection plot for paper figure.

Usage when in source/ directory for relative paths to be correct:
    $ python3 paper_figures_source/plot_rejection_plot.py

License:
    BSD 3-Clause License

    Copyright (c) 2023, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from typing import List, Set, Dict, Tuple, Optional, Union, Callable
from numpy.typing import NDArray

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# change to LaTeX style fonts
import matplotlib

matplotlib.rcParams["mathtext.fontset"] = "stix"
matplotlib.rcParams["font.family"] = "STIXGeneral"

# include parent directory in import path
import sys, os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

from filesystem_interaction import load_hdf
from useful_plotting_functions import BANG_WONG_COLOURS, colour_for_spec

output_plot_label = "fig6.pdf"
df = load_hdf("./paper_figures_source/data_files/rejection_rate.h5")

# networks with multiple runs will get overlaid by matplotlib below, luckily, the rejection rates are the same for each trial
# .replace("100Hzf-6", "$f^{-6}$")
formatted_specs = [
    rf"{spec}".replace("[", "")
    .replace("]", "")
    .replace("', '", ", ")
    .replace("'", "")
    .replace("A#-100Hzf-6", "$\mathrm{A}\#_\mathrm{3Sus}$")
    .replace("CE-20-PMO_AU", "CE 20-km (PMO; AU)")
    .replace("CE-40-CBO_AU", "CE 40-km (CBO; AU)")
    .replace("CE-20-CBO_AU", "CE 20-km (CBO; AU)")
    .replace("_AU", " (AU)")
    .replace("_H", " (H)")
    .replace("_L", " (L)")
    .replace("_I", " (I)")
    .replace("_C", " C)")
    .replace("_N", " N)")
    .replace("Voy-CBO", "Voyager")
    .replace("CE-20-PMO", "CE 20-km (PMO;")
    .replace("CE-40-CBO", "CE 40-km (CBO;")
    .replace("CE-20-CBO", "CE 20-km (CBO;")
    .replace("-km", " km")
    .replace("V+_V", "Virgo+")
    .replace("K+_K", "KAGRA+")
    .replace("ET_E", "ET")
    for spec in df["network_spec"]
]
number_of_unique_specs = len(list(set(formatted_specs)))

# use the same colours as the hat graphs
bar_colours = list(range(len(formatted_specs)))
bar_edge_colours = list(range(len(formatted_specs)))
for i, spec in enumerate(formatted_specs):
    bar_colours[i] = colour_for_spec(spec)
    bar_edge_colours[i] = "k"

# plot the rejection rates
plt.rcParams.update({"font.size": 18})
fig, ax = plt.subplots(figsize=(16, 11))
rects = ax.bar(
    formatted_specs,
    df["rejection_rate"],
    color=bar_colours,
    edgecolor=bar_edge_colours,
    zorder=3,
)
ax.tick_params(axis="x", labelrotation=90)
column_size = 0.6
ax.set_xlim((-column_size, number_of_unique_specs - 1 + column_size))
# plt.axis('tight')
ax.set_ylabel(r"rejection rate", fontsize=22)

major_ticks = np.arange(0, 1 + 0.1, 0.2)
ax.yaxis.set_ticks(major_ticks, minor=False)
ax.yaxis.set_ticklabels(["{:.0%}".format(tick) for tick in major_ticks], minor=False)

minor_ticks = major_ticks[:-1] + 0.1
ax.yaxis.set_ticks(minor_ticks, minor=True)

ax.set_ylim((0, 1))
# ax.set_yticklabels(['{:.0%}'.format(tick) for tick in ax.get_yticks()])
ax.grid(which="both", axis="y", color="gainsboro", zorder=0)

# legend, generated using dummy axes
fig2, ax2 = plt.subplots()
# colours = ["w", "r", "c", "g", "m"]
colours = [
    "w",
    BANG_WONG_COLOURS["Vermillion"],
    BANG_WONG_COLOURS["Sky blue"],
    BANG_WONG_COLOURS["Bluish green"],
    BANG_WONG_COLOURS["Reddish purple"],
]
labels = [
    # "no AU detector",
    # r"$\mathrm{A}\#_\mathrm{3Sus}$_AU",
    # "NEMO_AU",
    # "CE-20-PMO_AU",
    # "CE-20-CBO_AU",
    # "no observatory (AU)",
    "no AU observatory",
    r"$\mathrm{A}\#_\mathrm{3Sus}$ (AU)",
    "NEMO (AU)",
    # "CE 20-km (PMO; AU)",
    # "CE 20-km (CBO; AU)",
    "CE 20 km (PMO; AU)",
    "CE 20 km (CBO; AU)",
]
for colour, label in zip(colours, labels):
    ax2.bar(
        range(5),  # dummy value
        range(5),  # dummy value
        0.5,  # dummy value
        label=label,
        color=colour,
        edgecolor="black",
    )
handles, labels = ax2.get_legend_handles_labels()
ax.legend(
    handles,
    labels,
    fontsize=20,
    ncol=1,
    #     columnspacing=0.5,
    loc="upper right",
    #     bbox_to_anchor=(0, 0.95),
)

# * annotation
first_rect = rects[0]
first_height = df["rejection_rate"][0]
ax.annotate(
    "*",
    xy=(first_rect.get_x() + first_rect.get_width() / 2, first_height),
    xytext=(0, -30),
    textcoords="offset points",
    ha="center",
    va="bottom",
    fontsize=22,
)

fig.tight_layout()
fig.savefig(
    #     "./paper_figures_source/figures/rejections/" + "rejection_rate.pdf",
    "./paper_figures_source/figures/submission/" + output_plot_label,
    bbox_inches="tight",
)
# plt.show()
plt.close()
