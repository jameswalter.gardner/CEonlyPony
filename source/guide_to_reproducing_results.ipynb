{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "14655fc9",
   "metadata": {},
   "source": [
    "```\n",
    "Guide to using the codebase to generate the results and plots.\n",
    "\n",
    "License:\n",
    "    BSD 3-Clause License\n",
    "\n",
    "    Copyright (c) 2022, James Gardner.\n",
    "    All rights reserved except for those for the gwbench code which remain reserved\n",
    "    by S. Borhanian; the gwbench code is included in this repository for convenience.\n",
    "\n",
    "    Redistribution and use in source and binary forms, with or without\n",
    "    modification, are permitted provided that the following conditions are met:\n",
    "\n",
    "    1. Redistributions of source code must retain the above copyright notice, this\n",
    "       list of conditions and the following disclaimer.\n",
    "\n",
    "    2. Redistributions in binary form must reproduce the above copyright notice,\n",
    "       this list of conditions and the following disclaimer in the documentation\n",
    "       and/or other materials provided with the distribution.\n",
    "\n",
    "    3. Neither the name of the copyright holder nor the names of its\n",
    "       contributors may be used to endorse or promote products derived from\n",
    "       this software without specific prior written permission.\n",
    "\n",
    "    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\"\n",
    "    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE\n",
    "    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE\n",
    "    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE\n",
    "    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL\n",
    "    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR\n",
    "    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER\n",
    "    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,\n",
    "    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE\n",
    "    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b69b42d0",
   "metadata": {},
   "source": [
    "# Guide to using the codebase to replicate the results\n",
    "\n",
    "This notebook explains how to use this codebase on OzStar to generate all the results and plots to compare different networks of gravitational-wave detectors using `gwbench/`.\n",
    "\n",
    "`gwbench/` has been modified to allow placing a NEMO-Lite detector in Western Australia."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6293f058",
   "metadata": {},
   "source": [
    "### Minimal command to reproduce results and plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7d1122c2",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash \n",
    "sbatch slurm_job_scripts/job_main.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed8e77c9",
   "metadata": {},
   "source": [
    "Which launches the following slurm jobs, each of which starts only after all of the jobs from the previous line have finished successfully.\n",
    "- `sbatch slurm_job_scripts/job_set-up_for_calculating_injections.sh`\n",
    "- `sbatch slurm_job_scripts/job_calculate_unified_injections.sh`\n",
    "- `sbatch slurm_job_scripts/job_merge_and_cosmologically_resample_processed_data_task_files.sh`\n",
    "- `sbatch slurm_job_scripts/job_plot_detection_rate_and_measurement_errors.sh`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7fcd35b6",
   "metadata": {},
   "source": [
    "### Step-by-step explanation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff4caf6f",
   "metadata": {},
   "source": [
    "Each script is run on OzStar's farnarkle 1/2 login node. The time and memory requirements of each script are profiled by the scripts in `slurm_job_scripts/memory_profiling/`. Outputs, both stdout and stderr, from all slurm scripts are saved in `slurm_output_files/`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b70ba9a2",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sbatch slurm_job_scripts/job_set-up_for_calculating_injections.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dd8dd640",
   "metadata": {},
   "source": [
    "`$ sbatch slurm_job_scripts/job_set-up_for_calculating_injections.sh`\n",
    "- This script cleans and sets up the repo for a new run. \n",
    "- It calls four sub-processes:\n",
    "    - `bash ./delete_all_data_files.sh`\n",
    "        - This script deletes all of the injections data (raw, processed, and resampled) and other byproducts of previous runs excluding plots. \n",
    "    - `python3 ./generate_PSDs.py`\n",
    "        - This script generates the sensitivity curves in PSD for the A+ detectors with no sensitivity below a 100 or 200 Hz cutoff. These are saved in `gwbench/noise_curves`. To use these curves and the Western Australian locations, e.g. together as `A+-200Hz_WA`, the `gwbench/` scripts have been modified.\n",
    "    - `python3 ./generate_symbolic_derivatives.py`\n",
    "        - This script generates and saves the lambdified detector response derivatives for the symbolic waveforms into `lambdified_functions/` for all detector locations considered.\n",
    "    - `python3 ./generate_injections.py` \n",
    "        - This script generates the raw injection parameters for all science cases considered, generating 1.5 million samples uniformly in redshift for better resolution. The redshift dimension is split into six major bins, each with 250 thousand injections. The raw injections data files for each science case are saved in `data_raw_injections/` and split evenly across 2048 separate files in `data_raw_injections/task_files/`. Throughout the analysis, the data files are pandas dataframes saved as hdf5 (`.h5`) files which allows for mixed data types and headers, an advantage over the `.npy` format used previously."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f7ea40e",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sbatch slurm_job_scripts/job_calculate_unified_injections.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e099ba42",
   "metadata": {},
   "source": [
    "`$ sbatch slurm_job_scripts/job_calculate_unified_injections.sh`\n",
    "- This is the critical step which sets the long (~4 hour) timescale of the whole operation.\n",
    "- This job calls `run_as_task_calculate_unified_injections.py` for 2048 primitively parallel tasks. Each task processes one of the raw injection task files in `data_raw_injections/task_files/` uniformly through all of the networks considered using `calculate_unified_injections.py` (built off gwbench's multi-network feature). This is faster than calculating the shared detector responses multiple times and means that the processed data for each network --- which is saved in `data_processed_injections/task_files/` --- represents the same universe.\n",
    "- When an injection fails, e.g. due to an ill-conditioned FIM, in a given network, then the failure is noted in that network's data file and under a `False` flag of `unified_success` in all other networks. This means that, e.g., the detection rate plots can use all of the available successful injections for each network and, e.g., the histogram plots can restrict to only those injections that succeeded uniformly in all networks as to plot exactly the same universe each.\n",
    "- This script also uses the following modules:\n",
    "    - `filesystem_interaction.py` for input/output from the file system and information in filenames.\n",
    "    - `networks.py` for the list of considered networks.\n",
    "    - `network_subclass.py` to load the networks and set relevant attributes.\n",
    "    - `useful_functions.py` for commonly used but non--implementation specific functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "38e98a78",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sbatch slurm_job_scripts/job_merge_and_cosmologically_resample_processed_data_task_files.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dd3bb31a",
   "metadata": {},
   "source": [
    "`$ sbatch slurm_job_scripts/job_merge_and_cosmologically_resample_processed_data_task_files.sh`.\n",
    "- This script calls `run_as_task_merge_and_cosmologically_resample_processed_data_task_files.py` which combines the task files in `data_processed_injections/task_files/` into one data file in `data_processed_injections/` for each combination of network and science case, e.g. one file for the `A+_H..A+_L..V+_V..K+_K..A+_I` network and binary neutron-star science case drawn from 1024 task files given two total science cases.\n",
    "- To produce the physical distributions in later plots, the injections in `data_processed_injections/` that are uniform in redshift are resampled using a phenomenological cosmological distribution. This script then uses `cosmological_redshift_resampler.py` to resample the linearly uniformly sampled redshifts using a cosmological model and saves the results in `data_cosmologically_resampled_processed_injections/`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e1e0866",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sbatch slurm_job_scripts/job_plot_detection_rate_and_measurement_errors.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5c5c7efd",
   "metadata": {},
   "source": [
    "`$ sbatch slurm_job_scripts/job_plot_detection_rate_and_measurement_errors.sh`\n",
    "- This job calls `run_as_task_plot_collated_detection_rate_and_PDFs_and_CDFs.py` which runs `plot_collated_detection_rate.py` and `plot_collated_PDFs_and_CDFs.py` for each processed data file in `data_processed_injections/` to create plots in `plots/collated_eff_rate_vs_z/` and `plots/collated_PDFs_and_CDFs_snr_errs_sky-area/`, respectively.\n",
    "- This script also uses the following modules:\n",
    "    - `constants.py` for phenomenological values and signal-to-noise ratio thresholds.\n",
    "    - `merger_and_detection_rates.py` for the cosmological model.\n",
    "    - `results_class.py` to load the processed data files in `data_processed_injections/` and extract the relevant attributes.\n",
    "    - `useful_plotting_functions.py` for commonly used plotting functions."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd61436f",
   "metadata": {},
   "source": [
    "#### Troubleshooting:\n",
    "- Using %%bash magic inside Jupyter does not have access to local bash aliases."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
