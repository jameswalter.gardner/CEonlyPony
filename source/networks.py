"""Networks of gravitational-wave detectors of which to benchmark the performance.

Colours for plotting are drawn from B&S2022 and <https://flatuicolors.com/palette/us>.

Usage:
    See run_unified_injections_as_task.py.

License:
    BSD 3-Clause License

    Copyright (c) 2022, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from typing import List, Set, Dict, Tuple, Optional, Union

from useful_functions import flatten_list
from filesystem_interaction import network_spec_styler

# colours and linestyles pulled from B&S2022
BS2022_SIX = dict(
    nets=[
        ["A+_H", "A+_L", "V+_V", "K+_K", "A+_I"],
        ["V+_V", "K+_K", "Voyager-CBO_H", "Voyager-CBO_L", "Voyager-CBO_I"],
        ["A+_H", "A+_L", "K+_K", "A+_I", "ET_ET1", "ET_ET2", "ET_ET3"],
        ["V+_V", "K+_K", "A+_I", "CE2-40-CBO_C"],
        ["K+_K", "A+_I", "ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C"],
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C", "CE2-40-CBO_S"],
    ],
    colours=[
        "#8c510aff",
        "#bf812dff",
        "#dfc27dff",
        "#80cdc1ff",
        "#35978fff",
        "#01665eff",
    ],
    label="BS2022-six",
    linestyles=[
        (0, (1, 1)),
        (0, (5, 1, 1, 1, 1, 1)),
        (0, (5, 1, 1, 1)),
        (0, (5, 1)),
        (0, (1, 2)),
        "-",
    ],
)

# 'S' from B2021 was used as the Southern location above, henceforth I instead use 'WA' as the Western Australian location of the SKA with 'S''s orientation of the y-arm wrt Due East.
# TODO: update colours once network decided on
AUSTRALIA_2G = dict(
    nets=[
        ["A+-200Hz_AU", "A+_H", "A+_L", "V+_V", "K+_K", "A+_I"],
        ["A+-200Hz_AU", "A+_H", "A+_L", "V+_V", "K+_K"],
        ["A+-200Hz_AU", "A+_H", "A+_L", "V+_V"],
        ["A+-200Hz_AU", "A+_H", "A+_L"],
        ["A+_H", "A+_L", "V+_V", "K+_K", "A+_I"],
        ["A+_H", "A+_L", "V+_V", "K+_K"],
        ["A+_H", "A+_L", "V+_V"],
        ["A+_H", "A+_L"],
    ],
    # colours from https://flatuicolors.com/palette/au
    colours=[
        "#eb4d4b",
        "#6ab04c",
        "#be2edd",
        "#22a6b3",
        "#ff7979",
        "#badc58",
        "#e056fd",
        "#7ed6df",
    ],
    label="AUSTRALIA_2G",
)
# exploring frequency cuts with A+ and NEMO. Don't have a NEMO baseline yet, so compare to all 2G.
AUSTRALIA_2G_CUTS = dict(
    nets=[
        ["A+-100Hz_AU", "A+_H", "A+_L", "V+_V"],
        ["A+-100Hz_AU", "A+_H", "A+_L"],
        ["A+-50Hz_AU", "A+_H", "A+_L", "V+_V"],
        ["A+-50Hz_AU", "A+_H", "A+_L"],
        # A+ min is at 10 Hz
        ["A+_AU", "A+_H", "A+_L", "V+_V"],
        ["A+_AU", "A+_H", "A+_L"],
        ["NEMO-500Hz_AU", "A+_H", "A+_L", "V+_V", "K+_K", "A+_I"],
        ["NEMO-500Hz_AU", "A+_H", "A+_L", "V+_V", "K+_K"],
        ["NEMO-500Hz_AU", "A+_H", "A+_L", "V+_V"],
        ["NEMO-500Hz_AU", "A+_H", "A+_L"],
    ],
    # colours from https://flatuicolors.com/palette/au
    colours=[
        "#eb4d4b",
        "#ff7979",
        "#6ab04c",
        "#badc58",
        "#be2edd",
        "#e056fd",
        "#535c68",
        "#95afc0",
        "#c7ecee",
        "#dff9fb",
    ],
    label="AUSTRALIA_2G_CUTS",
)
# TODO: decide on Aus 2.5 and 3 G sets to run, and whether these SCs need the calibrated measurement errs
# AUSTRALIA_Voyager
# made a particular selection of 3G nets, perhaps should add background 2G/2.5G but need to account for number of available staff
# guess that if ET and 2 US CE are built, then there is no need for an Aus CE
AUSTRALIA_3G = dict(
    nets=[
        ["CE2-40-CBO_C", "CE2-20-PMO_AU"],
        # single detector network will probably fail a lot of unified injections, which might require reconstructing without it from success_bool, try with it for once
        ["CE2-40-CBO_C"],
        ["CE2-40-CBO_C", "CE2-20-CBO_N", "CE2-20-PMO_AU"],
        ["CE2-40-CBO_C", "CE2-20-CBO_N"],
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-20-PMO_AU"],
        ["ET_ET1", "ET_ET2", "ET_ET3"],
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C", "CE2-20-PMO_AU"],
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C"],
    ],
    colours=[
        "#eb4d4b",
        "#ff7979",
        "#6ab04c",
        "#badc58",
        "#be2edd",
        "#e056fd",
        "#22a6b3",
        "#7ed6df",
    ],
    label="AUSTRALIA_3G",
)
# 2.5 G, do the A+ cuts survive in a Voyager world?
# TODO: run this once we understand how the cuts work and whether to analyse A+ or NEMO and which cut to use for each
# from 2GCUTS, NEMO-500 Hz looks promising, run with all since I don't know what the cost profile against frequency looks like - Sep 9
AUSTRALIA_2HALFG = dict(
    nets=[
        ["A+-100Hz_AU", "Voyager-CBO_H", "Voyager-CBO_L", "V+_V"],
        ["A+-100Hz_AU", "Voyager-CBO_H", "Voyager-CBO_L"],
        ["A+-200Hz_AU", "Voyager-CBO_H", "Voyager-CBO_L", "V+_V"],
        ["A+-200Hz_AU", "Voyager-CBO_H", "Voyager-CBO_L"],
        ["NEMO-500Hz_AU", "Voyager-CBO_H", "Voyager-CBO_L", "V+_V"],
        ["NEMO-500Hz_AU", "Voyager-CBO_H", "Voyager-CBO_L"],
        ["Voyager-CBO_H", "Voyager-CBO_L", "V+_V"],
        ["Voyager-CBO_H", "Voyager-CBO_L"],
    ],
    # colours from https://flatuicolors.com/palette/au
    colours=[
        "#eb4d4b",
        "#ff7979",
        "#6ab04c",
        "#badc58",
        "#be2edd",
        "#e056fd",
        "#535c68",
        "#95afc0",
    ],
    label="AUSTRALIA_2HALFG",
)

# post-O5 2.5 G, since A+ is O5 and building a detector will take 10 years regardless.
# when A#_100Hzf-6 in Aus, A# at LIGO sites
# include India in LIGO sites, sprinkle in V+ and K+
AUSTRALIA_POSTO5 = dict(
    nets=[
        ["A#-100Hzf-6_AU", "A#_H", "A#_L", "V+_V", "K+_K", "A#_I"],
        ["A#-100Hzf-6_AU", "A#_H", "A#_L", "V+_V", "K+_K"],
        ["A#-100Hzf-6_AU", "A#_H", "A#_L", "V+_V"],
        #         ["A#-100Hzf-6_AU", "A#_H", "A#_L", "A#_I"],
        ["A#-100Hzf-6_AU", "A#_H", "A#_L"],
        ["A#_H", "A#_L", "V+_V", "K+_K", "A#_I"],
        ["A#_H", "A#_L", "V+_V", "K+_K"],
        ["A#_H", "A#_L", "V+_V"],
        #         ["A#_H", "A#_L", "A#_I"],
        ["A#_H", "A#_L"],
    ],
    colours=[
        "#eb4d4b",
        "#6ab04c",
        "#be2edd",
        "#22a6b3",
        "#ff7979",
        "#badc58",
        "#e056fd",
        "#7ed6df",
    ],
    label="AUSTRALIA_POSTO5",
)
# when NEMO (full) in Aus, Voy at LIGO sites
# use existing Voyager reference curves, need to generate two new ones
AUSTRALIA_NEMO_2HALFG = dict(
    nets=[
        ["NEMO_AU", "Voyager-CBO_H", "Voyager-CBO_L", "V+_V", "K+_K", "Voyager-CBO_I"],
        ["NEMO_AU", "Voyager-CBO_H", "Voyager-CBO_L", "V+_V", "K+_K"],
        ["NEMO_AU", "Voyager-CBO_H", "Voyager-CBO_L", "V+_V"],
        ["NEMO_AU", "Voyager-CBO_H", "Voyager-CBO_L"],
        ["Voyager-CBO_H", "Voyager-CBO_L", "V+_V", "K+_K", "Voyager-CBO_I"],
        ["Voyager-CBO_H", "Voyager-CBO_L", "V+_V", "K+_K"],
        #         ["Voyager-CBO_H", "Voyager-CBO_L", "V+_V"],
        #         ["Voyager-CBO_H", "Voyager-CBO_L"],
    ],
    colours=[
        "#eb4d4b",
        "#ff7979",
        "#6ab04c",
        "#badc58",
        "#be2edd",
        "#e056fd",
    ],
    label="AUSTRALIA_NEMO_2HALFG",
)
# use reference curves from AUSTRALIA_3G
# TODO: place a CE in India, no one has mentioned this to me?
AUSTRALIA_NEMO_3G = dict(
    nets=[
        ["NEMO_AU", "ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C"],
        ["NEMO_AU", "ET_ET1", "ET_ET2", "ET_ET3"],
        ["NEMO_AU", "CE2-40-CBO_C", "CE2-20-CBO_N"],
        ["NEMO_AU", "CE2-40-CBO_C"],
        #         ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C"],
        #         ["ET_ET1", "ET_ET2", "ET_ET3"],
        #         ["CE2-40-CBO_C", "CE2-20-CBO_N"],
        #         ["CE2-40-CBO_C"],
    ],
    colours=[
        "#eb4d4b",
        "#ff7979",
        "#6ab04c",
        "#badc58",
    ],
    label="AUSTRALIA_NEMO_3G",
)
# TODO: to compare to previous 3G reference curves need to unify universes between runs
# include reference curves here since the detectors are already in the set (which is fast using gwbench's unique tecs option) and to avoid doing the above unification/to have something to test the unification against (shouldn't be an issue).
AUSTRALIA_20kCBO = dict(
    nets=[
        ["CE2-40-CBO_C", "CE2-20-CBO_AU"],
        ["CE2-40-CBO_C", "CE2-20-CBO_N", "CE2-20-CBO_AU"],
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-20-CBO_AU"],
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C", "CE2-20-CBO_AU"],
        ["CE2-40-CBO_C"],
        ["CE2-40-CBO_C", "CE2-20-CBO_N"],
        ["ET_ET1", "ET_ET2", "ET_ET3"],
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C"],
    ],
    colours=[
        "#eb4d4b",
        "#6ab04c",
        "#be2edd",
        "#22a6b3",
        "#ff7979",
        "#badc58",
        "#e056fd",
        "#7ed6df",
    ],
    label="AUSTRALIA_20kCBO",
)
# test lifetime of Post 05 detector
# could include explicit reference curves to validate unification of universes later, deciding against given O(1) deviation in CBO results from PMO run
AUSTRALIA_POSTO5_2HALFG = dict(
    nets=[
        [
            "A#-100Hzf-6_AU",
            "Voyager-CBO_H",
            "Voyager-CBO_L",
            "V+_V",
            "K+_K",
            "Voyager-CBO_I",
        ],
        ["A#-100Hzf-6_AU", "Voyager-CBO_H", "Voyager-CBO_L", "V+_V", "K+_K"],
        ["A#-100Hzf-6_AU", "Voyager-CBO_H", "Voyager-CBO_L", "V+_V"],
        ["A#-100Hzf-6_AU", "Voyager-CBO_H", "Voyager-CBO_L"],
        #         ["Voyager-CBO_H", "Voyager-CBO_L", "V+_V", "K+_K", "Voyager-CBO_I"],
        #         ["Voyager-CBO_H", "Voyager-CBO_L", "V+_V", "K+_K"],
        #         ["Voyager-CBO_H", "Voyager-CBO_L", "V+_V"],
        #         ["Voyager-CBO_H", "Voyager-CBO_L"],
    ],
    colours=[
        "#eb4d4b",
        "#ff7979",
        "#6ab04c",
        "#badc58",
        #         "#ff7979",
        #         "#badc58",
        #         "#e056fd",
        #         "#7ed6df",
    ],
    label="AUSTRALIA_POSTO5_2HALFG",
)

# --- CE only ---
# - One in the US, removed this ill-conditioned network from the set since injection rejection is now unified.
# CE_C = dict(nets=[["CE2-40-CBO_C"]], colours=["#2d3436"], label="CE_C")
# - One in the US, One in Australia
CE_CS = dict(
    nets=[
        ["CE2-40-CBO_C", "CE2-40-CBO_AU"],
        ["CE2-40-CBO_C", "CE2-20-CBO_AU"],
        ["CE2-40-CBO_C", "CE2-20-PMO_AU"],
    ],
    colours=["#0984e3", "#74b9ff", "#ff7675"],
    label="CE_CS",
)
# - Two in the US --> done in B&S2022 with VKI+
CE_CN = dict(
    nets=[
        ["CE2-40-CBO_C", "CE2-40-CBO_N"],
        ["CE2-40-CBO_C", "CE2-20-CBO_N"],
        ["CE2-40-CBO_C", "CE2-20-PMO_N"],
    ],
    colours=["#0984e3", "#74b9ff", "#ff7675"],
    label="CE_CN",
)
# - Three (two in US, one in Aus)
CE_CNS = dict(
    nets=[
        ["CE2-40-CBO_C", "CE2-20-CBO_N", "CE2-40-CBO_AU"],
        ["CE2-40-CBO_C", "CE2-20-CBO_N", "CE2-20-CBO_AU"],
        ["CE2-40-CBO_C", "CE2-20-CBO_N", "CE2-20-PMO_AU"],
        ["CE2-40-CBO_C", "CE2-20-PMO_N", "CE2-40-CBO_AU"],
        ["CE2-40-CBO_C", "CE2-20-PMO_N", "CE2-20-CBO_AU"],
        ["CE2-40-CBO_C", "CE2-20-PMO_N", "CE2-20-PMO_AU"],
    ],
    colours=["#0984e3", "#74b9ff", "#ff7675", "#6c5ce7", "#a29bfe", "#fd79a8"],
    label="CE_CNS",
)

# --- CE_S with non-CE others ---
# - CE_S + A+ network
CE_S_AND_2G = dict(
    nets=[
        ["A+_H", "A+_L", "V+_V", "K+_K", "A+_I", "CE2-40-CBO_AU"],
        ["A+_H", "A+_L", "V+_V", "K+_K", "A+_I", "CE2-20-CBO_AU"],
        ["A+_H", "A+_L", "V+_V", "K+_K", "A+_I", "CE2-20-PMO_AU"],
    ],
    colours=["#00cec9", "#81ecec", "#fab1a0"],
    label="CE_S_and_2G",
)
# - CE_S + Voyager notework
CE_S_AND_VOYAGER = dict(
    nets=[
        [
            "V+_V",
            "K+_K",
            "Voyager-CBO_H",
            "Voyager-CBO_L",
            "Voyager-CBO_I",
            "CE2-40-CBO_AU",
        ],
        [
            "V+_V",
            "K+_K",
            "Voyager-CBO_H",
            "Voyager-CBO_L",
            "Voyager-CBO_I",
            "CE2-20-CBO_AU",
        ],
        [
            "V+_V",
            "K+_K",
            "Voyager-CBO_H",
            "Voyager-CBO_L",
            "Voyager-CBO_I",
            "CE2-20-PMO_AU",
        ],
    ],
    colours=["#00cec9", "#81ecec", "#fab1a0"],
    label="CE_S_and_Voy",
)
# - CE_S + ET, not yet examined, compare to CE_CN_AND_ET
CE_S_AND_ET = dict(
    nets=[
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_AU"],
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-20-CBO_AU"],
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-20-PMO_AU"],
    ],
    colours=["#00cec9", "#81ecec", "#fab1a0"],
    label="CE_S_and_ET",
)

# --- CE_CS with ET ---
CE_CS_AND_ET = dict(
    nets=[
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C", "CE2-40-CBO_AU"],
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C", "CE2-20-CBO_AU"],
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C", "CE2-20-PMO_AU"],
    ],
    colours=["#00cec9", "#81ecec", "#fab1a0"],
    label="CE_CS_and_ET",
)
# --- CE_CN with ET ---
CE_CN_AND_ET = dict(
    nets=[
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C", "CE2-40-CBO_N"],
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C", "CE2-20-CBO_N"],
        ["ET_ET1", "ET_ET2", "ET_ET3", "CE2-40-CBO_C", "CE2-20-PMO_N"],
    ],
    colours=["#00cec9", "#81ecec", "#fab1a0"],
    label="CE_CN_and_ET",
)

# --- H0 science case: to get sky localisation for the ~10 BNS this decade ---
# A+-200Hz is the A+ PSD with no sensitivity below 200 Hz (i.e. without needing four stages of suspension)
NEMOLF_AND_2G = dict(
    nets=[
        ["A+-200Hz_AU", "A+_H", "A+_L", "V+_V", "K+_K", "A+_I"],
        ["A+-200Hz_AU", "A+_H", "A+_L", "V+_V", "K+_K"],
        ["A+-200Hz_AU", "A+_H", "A+_L"],
    ],
    colours=["#00b894", "#55efc4", "#e17055"],
    label="NEMO-LF_and_2G",
)
# - NEMO-LF + Voyager notework
NEMOLF_AND_VOYAGER = dict(
    nets=[
        [
            "A+-200Hz_AU",
            "V+_V",
            "K+_K",
            "Voyager-CBO_H",
            "Voyager-CBO_L",
            "Voyager-CBO_I",
        ],
        [
            "A+-200Hz_AU",
            "V+_V",
            "K+_K",
            "Voyager-CBO_H",
            "Voyager-CBO_L",
        ],
        [
            "A+-200Hz_AU",
            "Voyager-CBO_H",
            "Voyager-CBO_L",
        ],
    ],
    colours=["#00cec9", "#81ecec", "#fab1a0"],
    label="CE_S_and_Voy",
)
# studying, e.g. NEMOLF and ET, seems unrealistic given the different decades of their use; TODO: remove CE_S_AND_2G for this reason as well?

# list of 11 network sets (dicts) to run unified tasks over
NET_DICT_LIST: List[
    Dict[str, Union[List[List[str]], List[str], str, List[Union[tuple, str]]]]
] = [
    BS2022_SIX,
    AUSTRALIA_2G,
    AUSTRALIA_3G,
    AUSTRALIA_2G_CUTS,
    #     CE_CS,
    #     CE_CN,
    #     CE_CNS,
    #     CE_S_AND_2G,
    #     CE_S_AND_VOYAGER,
    #     CE_S_AND_ET,
    #     CE_CS_AND_ET,
    #     CE_CN_AND_ET,
    #     NEMOLF_AND_2G,
    #     NEMOLF_AND_VOYAGER,
]
# list of network spec's within the broad set
NET_LIST = flatten_list([net_dict["nets"] for net_dict in NET_DICT_LIST])
# lookup table: given network_spec return colour, will override if same network_spec used twice
DICT_NETSPEC_TO_COLOUR = dict()
for net_dict in NET_DICT_LIST:
    for network_spec in net_dict["nets"]:
        # using network_spec's recovered from net_label in filename after applying net_label_styler
        DICT_NETSPEC_TO_COLOUR[network_spec_styler(network_spec)] = net_dict["colours"][
            net_dict["nets"].index(network_spec)
        ]
