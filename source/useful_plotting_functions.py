"""Miscellaneous tools for plotting that are not implementation specific.

License:
    BSD 3-Clause License

    Copyright (c) 2022, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from typing import List, Set, Dict, Tuple, Optional, Union, Type, Any
from numpy.typing import NDArray

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from constants import *

BANG_WONG_COLOURS = {
    "Black": "#000000",
    "Orange": "#E69F00",
    "Sky blue": "#56B4E9",
    "Bluish green": "#009E73",
    "Yellow": "#F0E442",
    "Blue": "#0072B2",
    "Vermillion": "#D55E00",
    "Reddish purple": "#CC79A7",
}


def colour_for_spec(spec: str) -> str:
    if "$\mathrm{A}\#_\mathrm{3Sus}$" in spec:
        # colour = "r"
        colour = BANG_WONG_COLOURS["Vermillion"]
    elif "NEMO" in spec:
        # colour = "c"
        colour = BANG_WONG_COLOURS["Sky blue"]
    elif "CE 20-km (PMO; AU)" in spec or "CE 20 km (PMO; AU)" in spec:
        # colour = "g"
        colour = BANG_WONG_COLOURS["Bluish green"]
    elif "CE 20-km (CBO; AU)" in spec or "CE 20 km (CBO; AU)" in spec:
        # colour = "m"
        colour = BANG_WONG_COLOURS["Reddish purple"]
    else:
        colour = "w"
    return colour


def plt_global_fontsize(fontsize: float) -> None:
    """Updates the matplotlib.pyplot fontsize globally, e.g. in axes labels, legends, etc..

    Args:
        fontsize: Fontsize.
    """
    plt.rcParams.update({"font.size": fontsize})


def force_log_grid(
    ax: Type[plt.Subplot],
    log_axis: str = "both",
    colour: str = "gainsboro",
    preserve_tick_labels: bool = True,
    **kwargs: Any,
) -> None:
    """Forces matplotlib to display all major and minor grid lines.

    If the plot would be too dense, then it will fail to do so.
    TODO: fix preserve_tick_labels, currently doesn't do anything, just calling fig.canvas.draw() before doesn't fix it.

    Args:
        ax: Axes to apply axes to.
        log_axis: Which of the x and y axes to apply a grid to.
        colour: Grid colour.
        preserve_tick_labels: Whether to preserve the existing tick labels. For this to work, the axis limits (e.g. xlim) and fig.canvas.draw() must be set beforehand.
        **kwargs: Options to pass to matplotlib.axes.Axes.grid.
    """
    #     if preserve_tick_labels:
    #         # this could be re-written with a decorator
    #         ax.set(ylim=ax.get_ylim(), xlim=ax.get_xlim())
    #         yticklabels, xticklabels = ax.yaxis.get_ticklabels(), ax.xaxis.get_ticklabels()
    #         #print(ax.get_ylim(), ax.get_xlim(), yticklabels, xticklabels)
    #         force_log_grid(ax, log_axis=log_axis, preserve_tick_labels=False, colour=color, **kwargs)
    #         ax.yaxis.set_ticklabels(yticklabels)
    #         ax.xaxis.set_ticklabels(xticklabels)
    #     else:
    ax.grid(which="both", axis="both", color=colour, **kwargs)
    # minorticks_on must be called before the locators by experimentation
    ax.minorticks_on()
    # TODO: fix this, current doesn't force loglog grid


#     if log_axis in ('both', 'x'):
#         ax.xaxis.set_major_locator(plt.LogLocator(base=10, numticks=10))
#         ax.xaxis.set_minor_locator(plt.LogLocator(base=10, subs='all', numticks=100))
#     if log_axis in ('both', 'y'):
#         ax.yaxis.set_major_locator(plt.LogLocator(base=10, numticks=10))
#         ax.yaxis.set_minor_locator(plt.LogLocator(base=10, subs='all', numticks=100))
#     # TODO: stop overcrowding of ticklabels, currently not working
#     print(len(ax.yaxis.get_ticklabels()), ax.get_yticks()[::2], len(ax.xaxis.get_ticklabels()), ax.get_xticks()[::2])
#     if len(ax.yaxis.get_ticklabels()) > 5:
#         ax.set(yticks=ax.get_yticks()[::2], ylim=ax.get_ylim())
#     if len(ax.xaxis.get_ticklabels()) > 5:
#         ax.set(xticks=ax.get_xticks()[::2], xlim=ax.get_xlim())


def add_SNR_contour_legend(ax: Type[plt.Subplot]) -> None:
    """Adds a contour legend a la Kuns+ 2020 to the given axes.

    Args:
        ax: Axes to which to add a legend to explain the contouring.
    """
    rect_pos = (0.05, 0.8)
    rect_width = 0.06
    rect_height = 0.1
    rect_top = (rect_pos[0], rect_pos[1] + rect_height)

    ax.add_patch(
        Rectangle(
            (rect_pos[0] - 0.005, rect_pos[1]),
            rect_width + 2 * 0.005,
            0.01,
            color="k",
            transform=ax.transAxes,
            zorder=10,
        )
    )
    ax.add_patch(
        Rectangle(
            rect_pos,
            rect_width,
            rect_height,
            color="darkgrey",
            transform=ax.transAxes,
            zorder=9,
        )
    )
    ax.add_patch(
        Rectangle(
            rect_top, rect_width, 0.001, color="k", transform=ax.transAxes, zorder=10
        )
    )
    ax.text(
        rect_pos[0] + rect_width + 0.015,
        rect_pos[1],
        f"SNR>{SNR_THRESHOLD_HI}",
        transform=ax.transAxes,
        horizontalalignment="left",
        verticalalignment="center",
    )
    ax.text(
        rect_top[0] + rect_width + 0.015,
        rect_top[1],
        f"SNR>{SNR_THRESHOLD_LO}",
        transform=ax.transAxes,
        horizontalalignment="left",
        verticalalignment="center",
    )


def plot_hat_graph(
    ax: Type[plt.Subplot],
    xlabels: List[str],
    values: NDArray,
    group_labels: List[str],
    annotations_off=False,
    colour_list: Optional[List[str]] = None,
    spacing_between_hats: float = 0.3,
    annotation_array: Optional[NDArray] = None,
    add_asterix_for_single_detector_network: bool = False,
    use_rectangles: bool = True,
    **kwargs,
):
    """
    Create a hat graph.
    From https://matplotlib.org/stable/gallery/lines_bars_and_markers/hat_graph.html#hat-graph

    Parameters
    ----------
    ax : matplotlib.axes.Axes
        The Axes to plot into.
    xlabels : list of str
        The category names to be displayed on the x-axis.
    values : (M, N) array-like
        The data values.
        Rows are the groups (len(group_labels) == M).
        Columns are the categories (len(xlabels) == N).
    group_labels : list of str
        The group labels displayed in the legend.
    annotation_fontsize : float
        Size of the annotations on each bar.
    annotations_off : bool
        Whether to display annotations on each bar.
    colour_list
        Colours for fill of each bar with the first value ignored.
    spacing_between_hats
        Xaxis spacing between the hats (groups of bars).
    annotation_array
        Annotations for each bar in the shape of ``values''.
    use_rectangles
        Whether to use rectangles or stems.
    kwargs
        Passed to label_bars, e.g. display_precision for the height of each bar.
    """

    def label_bars(
        heights,
        rects,
        display_precision=0,
        use_percentage=False,
        labels=None,
        annotation_fontsize=None,
        **kwargs,
    ):
        """Attach a text label on top of each bar."""
        for i, (height, rect) in enumerate(zip(heights, rects)):
            if labels is None:
                if use_percentage:
                    annotation = f"{height:.{display_precision}%}"  # .replace("%", "")
                else:
                    annotation = f"{height:.{display_precision}f}"
            else:
                annotation = labels[i]
            # vertical offset measured in points
            y_offset = 0
            if annotation_fontsize is None:
                # TODO: adaptively adjust the fontsize given the length of the annotation
                l = len(annotation)
                # fs_list = [16, 14, 12]
                fs_list = [18, 16, 14]
                if l <= 2:
                    fontsize = fs_list[0]
                elif ">" in annotation and l == 3:
                    fontsize = fs_list[1]
                elif ">" not in annotation and l == 3:
                    fontsize = fs_list[0]
                else:
                    fontsize = fs_list[2]
            else:
                fontsize = annotation_fontsize
            # if debug: print(annotation, l, fontsize, annotation_fontsize)
            ax.annotate(
                annotation,
                xy=(rect.get_x() + rect.get_width() / 2, height),
                xytext=(0, y_offset),
                textcoords="offset points",
                ha="center",
                va="bottom",
                fontsize=fontsize,
                # backgroundcolor='white',
            )

    values = np.asarray(values)
    x = np.arange(values.shape[1])

    xticks_options = dict(labels=xlabels, fontsize=22)
    if use_rectangles:
        # place ticks in the middle of each set of hats, x works for 2 hats, need to add width/2 for each additional hat thereafter
        number_of_hats = values.shape[0]
        number_of_hats_beyond_two = max(0, number_of_hats - 2)
        width = (1 - spacing_between_hats) / number_of_hats
        ax.set_xticks(
            x + width / 2 * number_of_hats_beyond_two, **xticks_options
        )
    else:
        # place tick at stem if one stem, or between stems if two stems
        number_of_stems = values.shape[0] - 1
        width = 2 * (1 - spacing_between_hats) / number_of_stems
        ax.set_xticks(x, **xticks_options)
        ax.tick_params(axis='x', which='major', pad=10)


    heights0 = values[0]
    # TODO: make default the plt default colour cycle
    if colour_list is None:
        colour_list = np.full((1, len(values)), "r")
    for i, (heights, group_label) in enumerate(zip(values, group_labels)):
        style = {"fill": False} if i == 0 else {"edgecolor": "black"}

        # hat graph
        if use_rectangles:
            rects = ax.bar(
                x - spacing_between_hats / 2 + i * width,
                heights - heights0,
                width,
                bottom=heights0,
                label=group_label,
                color=colour_list[i],
                zorder=3,
                **style,
            )

            # TODO: make this work with stems
            if not annotations_off:
                if annotation_array is not None:
                    label_bars(heights, rects, labels=annotation_array[i], **kwargs)
                else:
                    label_bars(heights, rects, labels=None, **kwargs)

            # a hack for 3G
            if (
                add_asterix_for_single_detector_network
                and group_label == "no observatory (AU)"
            ):
                # print("adding an asterix for 3G")
                height = heights[0]
                rect = rects[0]
                # print(group_label, rect, height)
                ax.annotate(
                    "*",
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 4),  # 4 points vertical offset.
                    textcoords="offset points",
                    ha="center",
                    va="bottom",
                    fontsize=18,
                )
        # stem graph
        else:
            # skip the reference network
            if i == 0:
                continue

            # list form to vectorise stems
            if number_of_stems == 1:
                x_list = x
            elif number_of_stems == 2:
                x_separation = 0.05
                if i == 1:
                    x_list = x - x_separation
                elif i == 2:
                    x_list = x + x_separation
            y0_list = heights0
            y1_list = heights
            colour = colour_list[i]
            label = group_label

            ax.vlines(
                x_list,
                y0_list,
                y1_list,
                linewidth=4,
                label=label,
                color=colour,
                zorder=3,
            )
            ax.scatter(x_list, y0_list, marker="_", color=colour, zorder=3, s=80)
            ax.scatter(x_list, y1_list, marker="^", color=colour, zorder=3, s=80)


def draw_brace(ax, xspan, yy, text, upsidedown=False, text_yy=None, **text_kwargs):
    """Source: <https://stackoverflow.com/a/74567450>
    Draws an annotated brace outside the axes."""
    xmin, xmax = xspan
    xspan = xmax - xmin
    ax_xmin, ax_xmax = ax.get_xlim()
    xax_span = ax_xmax - ax_xmin

    ymin, ymax = ax.get_ylim()
    yspan = ymax - ymin
    resolution = int(xspan / xax_span * 100) * 2 + 1  # guaranteed uneven
    beta = 300.0 / xax_span  # the higher this is, the smaller the radius

    x = np.linspace(xmin, xmax, resolution)
    x_half = x[: int(resolution / 2) + 1]
    y_half_brace = 1 / (1.0 + np.exp(-beta * (x_half - x_half[0]))) + 1 / (
        1.0 + np.exp(-beta * (x_half - x_half[-1]))
    )
    if upsidedown:
        y = np.concatenate((y_half_brace[-1:0:-1], y_half_brace))
    else:
        y = np.concatenate((y_half_brace, y_half_brace[-2::-1]))
    y = yy + (0.05 * y - 0.01) * yspan  # adjust vertical position

    ax.autoscale(False)
    ax.plot(x, -y, color="black", lw=1, clip_on=False)

    if text_yy is None:
        text_yy = yy
    if upsidedown:
        ax.text(
            (xmax + xmin) / 2.0,
            -text_yy + 0.01 * yspan,
            text,
            ha="center",
            va="bottom",
            **text_kwargs,
        )
    else:
        ax.text(
            (xmax + xmin) / 2.0,
            -text_yy - 0.17 * yspan,
            text,
            ha="center",
            va="bottom",
            **text_kwargs,
        )
