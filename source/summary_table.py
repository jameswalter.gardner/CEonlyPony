"""Summary table to display summary statistics of cosmologically resampled injections.

License:
    BSD 3-Clause License

    Copyright (c) 2022, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
from typing import List, Set, Dict, Tuple, Optional, Union, Any
import pandas as pd
import os
from IPython.display import HTML, display

from constants import *
from filesystem_interaction import file_name_to_multiline_readable


def dataframe_colour_cols(column: Any, col_colour_dict: dict):
    """Colour the column of a dataframe.

    From <https://stackoverflow.com/a/41655004>

    Args:
        column: Column series of a pandas dataframe.
        col_colour_dict: Colours to display columns with.
    """
    if column.name in col_colour_dict.keys():
        return ["background-color: {}".format(col_colour_dict[column.name])] * len(
            column
        )
    return [""] * len(column)


def summary_table(
    files: Optional[List[str]] = None,
    observation_time_in_years: float = 10.0,
    return_table: bool = False,
    title: str = "Table: Summary of counts per year for EM follow-up of BNS mergers.",
    save_path: Optional[str] = None,
    col_colour_dict: dict = {"loose metric": "lightgreen", "tight metric": "lightblue"},
    regenerate_table: bool = False,
    display_table: bool = True,
    display_precision: int = 0,
    save_html: bool = True,
    reliability_thresholds: bool = False,
) -> None:
    """Prints a summary table of the detection metrics for a set of networks.

    Summary table for Bram and easy reference
    Like tables in B&S2022, show distribution with SNR and EM followup.
    TODO: check the effect of the resampling. B&S say that numbers less than 10 are susceptible in any such table.
    From B&S2022: "Due to uncertainty in the various quantities that go into the calculation these numbers are no more accurate than one or two significant figures." Which means that the single-digit values are highly variable.

    Args:
        files: Resampled injections of networks to compare, include relative path. If files is None, then load the existing table from save_path.
        observation_time_in_years: Number of years that the resampling was taken over.
        return_table: Whether to return the dataframe.
        title: Label to display the table with.
        save_path: Where to save the styled table to if given.
        col_colour_dict: Colours to display columns with.
        regenerate_table: Whether to regenerate the table if it already exists.
        display_table: Whether to display the styled table.
        display_precision: Number of decimal places to display counts to.
        save_html: Whether to save an .HTML file of the styled table (doesn't match the nice Jupyter output).
        reliability_thresholds: Whether to use the reliability SNR thresholds and condition on sources in-range.
    """
    # check that save_path is well-defined
    if save_path is not None and not os.path.exists(os.path.dirname(save_path)):
        raise ValueError(f"save_path directory: {save_path} does not exist.")
    # if the summary table already exists, then load it. optionally, regenerate it.
    if save_path is not None and os.path.exists(save_path) and not regenerate_table:
        table = pd.read_hdf(save_path, key="df")
    else:
        # create the summary table
        data = []
        for i, file in enumerate(files):
            counts = {}
            df = pd.read_hdf(file, key="df")
            network_styled = file_name_to_multiline_readable(
                file, net_only=True, no_net_header=True
            )

            # old style of table
            if not reliability_thresholds:
                # weakest followupable, i.e. loosest conditions on SNR, sky area, and distance
                counts["loose_metric"] = len(
                    df.loc[
                        (df["snr"] > SNR_THRESHOLD_LO)
                        & (df["sky_area_90"] < EM_FOLLOWUP_SKY_AREA_SQR_DEG_HI)
                        & (df["redshift"] < EM_FOLLOWUP_REDSHIFT_HI)
                    ]
                )
                counts["tight_metric"] = len(
                    df.loc[
                        (df["snr"] > SNR_THRESHOLD_LO)
                        & (df["sky_area_90"] < EM_FOLLOWUP_SKY_AREA_SQR_DEG_MID)
                        & (df["redshift"] < EM_FOLLOWUP_REDSHIFT_HI)
                    ]
                )
                #             # to test whether the DL constraint is degenerate
                #             counts["too_far_for_followup"] = len(
                #                 df.loc[
                #                     (df["snr"] > SNR_THRESHOLD_LO)
                #                     & (df["sky_area_90"] < EM_FOLLOWUP_SKY_AREA_SQR_DEG_HI)
                #                     & (df["redshift"] > EM_FOLLOWUP_REDSHIFT_HI)
                #                 ]
                #             )
                counts["detected_lo"] = len(df.loc[df["snr"] > SNR_THRESHOLD_LO])
                counts["detected_mid"] = len(df.loc[df["snr"] > SNR_THRESHOLD_MID])
                counts["detected_hi"] = len(df.loc[df["snr"] > SNR_THRESHOLD_HI])
                # named in inverse localisation, i.e. localised_hi is the highest area
                counts["localised_hi"] = len(
                    df.loc[df["sky_area_90"] < EM_FOLLOWUP_SKY_AREA_SQR_DEG_HI]
                )
                counts["localised_mid"] = len(
                    df.loc[df["sky_area_90"] < EM_FOLLOWUP_SKY_AREA_SQR_DEG_MID]
                )
                counts["localised_lo"] = len(
                    df.loc[df["sky_area_90"] < EM_FOLLOWUP_SKY_AREA_SQR_DEG_LO]
                )
                # this parameter requires varying. if the simulations are from the same population then this will be constant as it is an input parameter
                counts["in_range_lo"] = len(
                    df.loc[df["redshift"] < EM_FOLLOWUP_REDSHIFT_LO]
                )
                counts["in_range_hi"] = len(
                    df.loc[df["redshift"] < EM_FOLLOWUP_REDSHIFT_HI]
                )
                counts["total"] = len(df)
            # new style of table
            else:
                # only use data in-range
                df_filt = df.loc[df["redshift"] < EM_FOLLOWUP_REDSHIFT_HI]

                # 12, 15, 25
                SNR_thresholds = [
                    SNR_RELIABLE_THRESHOLD_LO,
                    SNR_RELIABLE_THRESHOLD_MID,
                    SNR_RELIABLE_THRESHOLD_HI,
                ]
                for SNR_thr in SNR_thresholds:
                    column = f"SNR > {SNR_thr:.0f}"
                    counts[column] = len(df_filt.loc[df_filt["snr"] > SNR_thr])

                # 10, 1; in sqr deg henceforth
                sky_thresholds = [
                    EM_FOLLOWUP_SKY_AREA_SQR_DEG_HI,
                    EM_FOLLOWUP_SKY_AREA_SQR_DEG_MID,
                ]
                for sky_thr in sky_thresholds:
                    column = f"sky < {sky_thr:.0f}"
                    counts[column] = len(df_filt.loc[df_filt["sky_area_90"] < sky_thr])

                # cross terms
                for SNR_thr in SNR_thresholds:
                    for sky_thr in sky_thresholds:
                        column = f"SNR > {SNR_thr:.0f}, sky < {sky_thr:.0f}"
                        counts[column] = len(
                            df_filt.loc[
                                (df_filt["snr"] > SNR_thr)
                                & (df_filt["sky_area_90"] < sky_thr)
                            ]
                        )
                # [
                #  'SNR > 12',
                #  'SNR > 15',
                #  'SNR > 25',
                #  'sky < 10',
                #  'sky < 1',
                #  'SNR > 12, sky < 10',
                #  'SNR > 12, sky < 1',
                #  'SNR > 15, sky < 10',
                #  'SNR > 15, sky < 1',
                #  'SNR > 25, sky < 10',
                #  'SNR > 25, sky < 1'
                # ]

            # scale all counts for counts per year, scale here to avoid dealing with mixed dtypes
            for key, value in counts.items():
                counts[key] = value / observation_time_in_years

            counts["network"] = network_styled
            data.append(counts)

        # don't loop over and deposit into an empty dataframe, just create the dataframe at the end instead <https://stackoverflow.com/a/13786327>
        table = pd.DataFrame(data)
        table.set_index("network", inplace=True)
        if save_path is not None:
            table.to_hdf(save_path, key="df", format="table")
    #             print(table.to_latex())

    # display (ipython) the summary table
    # TODO: make this work for reliability_thresholds = True
    if not reliability_thresholds:
        table.rename(
            columns={
                "loose_metric": "loose metric",
                "tight_metric": "tight metric",
                #             # legacy renames
                #             "followupable": 'loose metric',
                #             "too_far_for_followup": 'too far for loose metric',
                #             "too_far_for_followup": f"$\mathrm{{SNR}} > {SNR_THRESHOLD_LO}, \Omega_{{90\%}} < {EM_FOLLOWUP_SKY_AREA_SQR_DEG_HI}^{{(\circ^2)}}, D_L > {EM_FOLLOWUP_LUMINOSITY_DISTANCE_MPC_HI} \,\mathrm{{Mpc}}$",
                "detected_lo": f"SNR  > {SNR_THRESHOLD_LO:.0f}",
                "detected_mid": f"SNR > {SNR_THRESHOLD_MID:.0f}",
                "detected_hi": f"SNR > {SNR_THRESHOLD_HI:.0f}",
                "localised_lo": f"sky area < {EM_FOLLOWUP_SKY_AREA_SQR_DEG_LO} deg^2",
                "localised_mid": f"sky area < {EM_FOLLOWUP_SKY_AREA_SQR_DEG_MID:.0f} deg^2",
                "localised_hi": f"sky area < {EM_FOLLOWUP_SKY_AREA_SQR_DEG_HI:.0f} deg^2",
                "in_range_lo": f"$D_L < {EM_FOLLOWUP_LUMINOSITY_DISTANCE_MPC_LO} \,\mathrm{{Mpc}}$",
                "in_range_hi": f"$D_L < {EM_FOLLOWUP_LUMINOSITY_DISTANCE_MPC_HI} \,\mathrm{{Mpc}}$",
                #             "followupable": 'metric',#f"$\mathrm{{metric}}:\;\mathrm{{SNR}} > {SNR_THRESHOLD_LO}, \Omega_{{90\%}} < {EM_FOLLOWUP_SKY_AREA_SQR_DEG_HI}^{{(\circ^2)}}, D_L < {EM_FOLLOWUP_LUMINOSITY_DISTANCE_MPC_HI} \,\mathrm{{Mpc}}$",
                #             "too_far_for_followup": f"$\mathrm{{SNR}} > {SNR_THRESHOLD_LO}, \Omega_{{90\%}} < {EM_FOLLOWUP_SKY_AREA_SQR_DEG_HI}^{{(\circ^2)}}, D_L > {EM_FOLLOWUP_LUMINOSITY_DISTANCE_MPC_HI} \,\mathrm{{Mpc}}$",
                #             "detected_lo": f"$\mathrm{{SNR}} > {SNR_THRESHOLD_LO}$",
                #             "detected_mid": f"$\mathrm{{SNR}}> {SNR_THRESHOLD_MID}$",
                #             "detected_hi": f"$\mathrm{{SNR}}> {SNR_THRESHOLD_HI}$",
                #             "localised_lo": f"$\Omega_{{90\%}} < {EM_FOLLOWUP_SKY_AREA_SQR_DEG_LO}^{{(\circ^2)}}$",
                #             "localised_mid": f"$\Omega_{{90\%}} < {EM_FOLLOWUP_SKY_AREA_SQR_DEG_MID}^{{(\circ^2)}}$",
                #             "localised_hi": f"$\Omega_{{90\%}} < {EM_FOLLOWUP_SKY_AREA_SQR_DEG_HI}^{{(\circ^2)}}$",
                #             "in_range_lo": f"$D_L < {EM_FOLLOWUP_LUMINOSITY_DISTANCE_MPC_LO} \,\mathrm{{Mpc}}$",
                #             "in_range_hi": f"$D_L < {EM_FOLLOWUP_LUMINOSITY_DISTANCE_MPC_HI} \,\mathrm{{Mpc}}$",
            },
            inplace=True,
        )

        # there is something funny about df object vs df.style object but this seems to work
        source_counts_in_distance = {
            "total": table["total"][0],
            "in close range": table[
                f"$D_L < {EM_FOLLOWUP_LUMINOSITY_DISTANCE_MPC_LO} \,\mathrm{{Mpc}}$"
            ][0],
            "in max range": table[
                f"$D_L < {EM_FOLLOWUP_LUMINOSITY_DISTANCE_MPC_HI} \,\mathrm{{Mpc}}$"
            ][0],
        }
        title = (
            title
            + f"\nSources per year, total: {source_counts_in_distance['total']:.{display_precision}f}, $D_L < {EM_FOLLOWUP_LUMINOSITY_DISTANCE_MPC_LO:.0f} \,\mathrm{{Mpc}}$: {source_counts_in_distance['in close range']:.{display_precision}f}, $D_L < {EM_FOLLOWUP_LUMINOSITY_DISTANCE_MPC_HI:.0f} \,\mathrm{{Mpc}}$: {source_counts_in_distance['in max range']:.{display_precision}f}."
        )

        # removing constant distance columns
        table = table.iloc[:, :8]

        if table.index.has_duplicates:
            table = table.reset_index()

        # style table
        title_style = [
            dict(selector="th", props=[("text-align", "right")]),
            {
                "selector": "caption",
                "props": [("color", "black"), ("font-size", "20px")],
            },
        ]
        # re-label for display
        table_to_style_later = table.rename(
            columns={
                # legacy renames
                "followupable": "loose metric",
                #             "too_far_for_followup": 'too far for loose metric',
            }
        )
        # drop too_far_for_followup if present
        if "too_far_for_followup" in table_to_style_later.columns:
            table_to_style_later.drop("too_far_for_followup", axis=1, inplace=True)
        table_styled = (
            table_to_style_later.style.format(precision=display_precision)
            .set_caption(title)
            .set_table_styles(title_style)
            .apply(dataframe_colour_cols, col_colour_dict=col_colour_dict)
        )
        #     TODO: try also saving the styled table using pickling, e.g.
        #         with open(save_dir + 'hat_unification_rate.pkl', 'wb') as output_file:
        #             pickle.dump(table_styled, output_file)
        #         with open(save_dir + 'hat_unification_rate.pkl', 'wb') as output_file:
        #             table_styled = pickle.load(output_file)

        if display_table:
            display(table_styled)

        if save_path is not None and save_html:
            with open(save_path.replace(".h5", ".html"), "w") as file:
                file.write(HTML(table_styled.render()).data)

        if return_table:
            # do not return table_styled as it is not accessible as a dataframe
            return (table, source_counts_in_distance)
    elif reliability_thresholds and return_table:
        return table
