#!/usr/bin/env python3
"""Merges (collates) data files from slurm tasks, e.g. processed injections data, into one data file.

Usage:
    Defaults to targetting processed injections data.
    To merge without deleting task files:
    $ python3 merge_processed_injections_task_files.py
    or
    $ python3 merge_processed_injections_task_files.py 0
    
    To merge and delete task files:
    $ python3 merge_processed_injections_task_files.py 1

License:
    BSD 3-Clause License

    Copyright (c) 2022, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from typing import List, Set, Dict, Tuple, Optional, Union
import numpy as np
import pandas as pd
import glob
import os, sys


def file_tag_from_task_file(file: str, cut_num_injs: bool = False) -> str:
    """Returns the file tag from a task output filename.

    Args:
        file: Filename of task file with or without path.
        cut_num_injs: Whether to not include the number of injections per redshift bin.
    """
    # TODO: update to using Pathlib?
    file_without_path = file[file.find("NET") :]
    file_tag = file_without_path.split("_TASK_")[0]
    if cut_num_injs:
        return file_tag.split("_INJS-PER-ZBIN_")[0]
    else:
        return file_tag


def merge_npy_files(
    output_filename: str,
    input_files: Optional[List[str]] = None,
    pattern: Optional[str] = None,
    input_path: Optional[str] = None,
    delete_input_files: bool = False,
) -> None:
    """Merges input or found .npy files into one .npy file.

    The injections data used to be saved as .npy but has now been updated to .h5 (May 17 2022).

    Args:
        output_filename: Filename of output collated .npy data file with path.
        input_files: Filenames of input .npy files.
        pattern: Pattern to search for input .npy files if input_files isn't given.
        input_path: Path to where to search for pattern if given and input_files isn't.
        delete_input_files: Whether to delete the input files if no error raised.

    Raises:
        Exception: If something goes wrong when concatenating arrays, which could be due to memory allocation.
    """
    # https://stackoverflow.com/questions/44164917/concatenating-numpy-arrays-from-a-directory
    # sorted to make debugging printout easier to read, also makes program deterministic
    if input_files is None:
        input_files = sorted(glob.glob(input_path + pattern))
    arrays = []
    for input_file in input_files:
        arrays.append(np.load(input_file))
    # try to merge and if fails then don't delete input files
    try:
        # if using empty data arrays, then ensure that they have the correct shape=(0, n)
        merged_array = np.concatenate(arrays)
        np.save(output_filename, merged_array)
    except:
        raise Exception(
            "Something went wrong when concatenating arrays, check memory allocation."
        )
    else:
        if delete_input_files:
            for input_file in input_files:
                os.remove(input_file)


def merge_h5_files(
    output_filename: str,
    input_files: List[str],
    delete_input_files: bool = False,
    sort_by_injection_index: bool = True,
) -> None:
    """Merges input or found .h5 files of pandas dataframes into one .h5 file.

    Args:
        output_filename: Filename of output collated .h5 data file with path.
        input_files: Filenames of input .h5 files with path.
        delete_input_files: Whether to delete the input files if no error raised.
        sort_by_injection_index: Whether to sort the rows by the injection index before saving.

    Raises:
        Exception: If something goes wrong when concatenating dataframes, which could be due to memory allocation.
    """
    try:
        with pd.HDFStore(output_filename) as store:
            for input_file in input_files:
                df = pd.read_hdf(input_file, key="df")
                store.append(
                    key="df",
                    value=df,
                    format="table",
                    min_itemsize={"rejection_flag": 40},
                )
            # each task file has 1464+ injections indexed in pandas from 0 so that the appended dataframe has repeated indices and index=False doesn't fix this; if all is well the index should match the injection index column; inplace=True is not working with saved .h5 file, just modifies in place in memory but not in storage?
            if sort_by_injection_index:
                df = (
                    store.get("df")
                    .sort_values(by=["injection_index"])
                    .reset_index(drop=True)
                )
            else:
                df = store.get("df").reset_index(drop=True)
            store.put("df", df)
    except:
        raise Exception(
            "Something went wrong when concatenating dataframes, check memory allocation."
        )
    else:
        if delete_input_files:
            for input_file in input_files:
                os.remove(input_file)


def merge_all_task_files(
    input_path: str = "./data_processed_injections/task_files/",
    pattern: str = "NET_*_SCI-CASE_*_WF_*_INJS-PER-ZBIN_*_TASK_*.h5",
    output_path: str = "./data_processed_injections/",
    delete_input_files: bool = False,
    skip_if_output_already_exists: bool = False,
    file_extension: str = ".h5",
    debug: bool = False,
) -> None:
    """Merges all processed data files from slurm tasks into one file per network and science case combination.

    Args:
        input_path: Path to processed data files from slurm tasks.
        pattern: Pattern to match input task files.
        output_path: Path to save merged data files.
        delete_input_files: Whether to delete the input task files after successful merging.
        skip_if_output_already_exists: Whether to skip merging if the output file already exists, i.e. to avoid overwriting a previous run.
        file_extension: Extension of input and output files, '.h5' or '.npy' for legacy.
        debug: Whether to print debug statements.
    """
    # sorting by TASK ID to make merged files deterministic: built from injections in the original order but with some missing
    task_files = sorted(
        glob.glob(input_path + pattern),
        key=lambda file: int(file.replace(file_extension, "_TASK_").split("_TASK_")[1]),
    )
    # split into separate network+sc+wf combinations
    # dict(tag1=[net1-task1, net1-task2], tag2=[net2-task1, net2-task2], ...)
    dict_tag_task_files: Dict[str, List[str]] = dict()
    for file in task_files:
        # remove num_injs from file_tag to capture the last injection task which contains more injections since 1024 doesn't divide the injections remaining after initial filtering
        file_tag_net_sc_wf = file_tag_from_task_file(file, cut_num_injs=True)
        # if it is not already in the dict, then find all matches and add them
        if not file_tag_net_sc_wf in dict_tag_task_files.keys():
            dict_tag_task_files[file_tag_net_sc_wf] = [
                file
                for file in task_files
                if file_tag_net_sc_wf
                == file_tag_from_task_file(file, cut_num_injs=True)
            ]

    for file_tag_net_sc_wf, task_files_same_tag in dict_tag_task_files.items():
        # assuming tasks all for the same run of injections
        input_num_injs = (
            task_files_same_tag[0]
            .replace(file_extension, "")
            .replace("_TASK_", "_INJS-PER-ZBIN_")
            .split("_INJS-PER-ZBIN_")[1]
        )
        output_filename = (
            output_path
            + f"{file_tag_net_sc_wf}_INJS-PER-ZBIN_{input_num_injs}"
            + file_extension
        )
        if skip_if_output_already_exists and os.path.exists(output_filename):
            return
        if debug:
            print(output_filename, task_files_same_tag, delete_input_files)
        if file_extension == ".npy":
            merge_npy_files(
                output_filename,
                input_files=task_files_same_tag,
                delete_input_files=delete_input_files,
            )
        elif file_extension == ".h5":
            merge_h5_files(
                output_filename=output_filename,
                input_files=task_files_same_tag,
                delete_input_files=delete_input_files,
            )
        else:
            raise ValueError("File extension not recognised.")


if __name__ == "__main__":
    # TODO: add progress bar, parallelise merging
    if len(sys.argv[1:]) == 1:
        delete_input_files = int(sys.argv[1])
    else:
        delete_input_files = 0
    merge_all_task_npy_files(delete_input_files=bool(delete_input_files))
