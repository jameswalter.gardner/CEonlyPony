#!/usr/bin/env python3
"""Merges the processed injections data task files and cosmologically resamples the result to produce physical data for plotting.

This should be parallelised using slurm, because the time per task is around a minute and there are 78 tasks.

Usage:
    Called in a job array by a slurm bash script, e.g.
    $ python3 run_as_task_merge_and_cosmologically_resample_processed_data_task_files.py TASK_ID

License:
    BSD 3-Clause License

    Copyright (c) 2022, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
from typing import List, Set, Dict, Tuple, Optional, Union
import glob
import numpy as np
import sys

from networks import *
from filesystem_interaction import network_spec_to_net_label
from merge_processed_injections_task_files import merge_all_task_files
from results_class import InjectionResults
from cosmological_redshift_resampler import (
    resample_redshift_cosmologically_from_results,
)


def file_tag_from_merge_task_id(
    task_id: int,
    network_specs: List[List[str]],
    num_injs_per_redshift_bin: int,
    science_case: Optional[str],
) -> str:
    """Returns the file label ("tag"), i.e. NET_{...}_SCI-CASE_{...}_WF_{...}_INJS-PER-ZBIN_{...}, of the task files to merge and resample.

    Args:
        task_id: Slurm task ID of the merge and resample job out of 1--78 (39 networks times 2 science cases) not of the 1--2048 tasks data files targetted.
        network_specs: Networks to search over.
        num_injs_per_redshift_bin: Initial total number of injections per major redshift bin from generating injections.
        science_case: Science case to study if there's only one.

    Raises:
        ValueError: If the science case is not recognised.
    """
    # TODO: overhaul this for single science case analyses
    if science_case is None:
        # net1-sc1, net1-sc2, net2-sc1, net2-sc2, ...
        network_spec = network_specs[(task_id - 1) // 2]
        science_case = ("BNS", "BBH")[(task_id - 1) % 2]
    else:
        # net1-sc1, net2-sc1, ...
        network_spec = network_specs[task_id - 1]

    network_label = network_spec_to_net_label(network_spec, styled=True)

    if science_case == "BNS":
        wf_model_name, wf_other_var_dic = (
            "lal_bns",
            dict(approximant="IMRPhenomD_NRTidalv2"),
        )
        # symbolic waveform
    #         wf_model_name, wf_other_var_dic = (
    #             "tf2_tidal",
    #             None,
    #         )
    elif science_case == "BBH":
        wf_model_name, wf_other_var_dic = "lal_bbh", dict(approximant="IMRPhenomHM")
    else:
        raise ValueError("Science case not recognised.")

    file_tag_partial = f"NET_{network_label}_SCI-CASE_{science_case}_WF_{wf_model_name}"
    if wf_other_var_dic is not None:
        file_tag_partial += f'_{wf_other_var_dic["approximant"]}'
    return file_tag_partial + f"_INJS-PER-ZBIN_{num_injs_per_redshift_bin}"


# TODO: use a single user input .txt file for the whole pipeline
# --- user input
debug = False
task_id = int(sys.argv[1])
# TODO: automate finding this number
num_injs_per_redshift_bin = 250000
norm_tag = "GWTC2"
seed = 12345

# science_case = None
# network_specs = NET_LIST

# --- AUSTRALIA BNS ---
science_case = "BNS"
# network_specs = AUSTRALIA_2G["nets"]
# network_specs = AUSTRALIA_3G["nets"]
# network_specs = AUSTRALIA_2G_CUTS["nets"]
# network_specs = AUSTRALIA_2HALFG["nets"]
# network_specs = AUSTRALIA_POSTO5["nets"]
# network_specs = AUSTRALIA_NEMO_2HALFG["nets"]
# network_specs = AUSTRALIA_NEMO_3G["nets"]
# network_specs = AUSTRALIA_20kCBO["nets"]
network_specs = AUSTRALIA_POSTO5_2HALFG["nets"]

# ---

# filesystem
task_files_path = "./data_processed_injections/task_files/"
merged_file_path = "./data_processed_injections/"
resampled_file_path = "./data_cosmologically_resampled_processed_injections/"

file_tag = file_tag_from_merge_task_id(
    task_id, network_specs, num_injs_per_redshift_bin, science_case
)
pattern = f"{file_tag}_TASK_*.h5"
# merge all 1024 processed injections data task files in ./data_processed_injections/task_files/ for the given network and science case into one file in ./data_processed_injections/; either delete previous runs manually or set skip_if_output_already_exists to False below
merge_all_task_files(
    input_path=task_files_path,
    pattern=pattern,
    output_path=merged_file_path,
    delete_input_files=False,
    skip_if_output_already_exists=True,
    file_extension=".h5",
)
# file name includes local path
merged_filename = merged_file_path + f"{file_tag}.h5"

# using single seed from user to seed all other processes
seed_sequence = np.random.SeedSequence(seed).spawn(2)
# seeds the sampling to determine the number of sources in each redshift bin from the cosmological model; rv_discrete can't handle random.SeedSequence but can handle random.Generator
cosmological_seed = np.random.default_rng(seed_sequence[0])
# seeds the resampling of the uniformly distributed results data to have the above number of sources in each bin, will be grandchildren of the user's SeedSequence and therefore can handle random.SeedSequence
resampling_seed = seed_sequence[1]

# cosmologically resampling the merged processed injections data file in ./data_processed_injections/ and saving the result into ./data_cosmologically_resampled_processed_injections/
# file name includes local path
resampled_filename = resampled_file_path + f"{file_tag}.h5"
if debug:
    print(f"input: {merged_filename}, output: {resampled_filename}")
# rejections are unified right now
results = InjectionResults(
    merged_filename, data_path="", norm_tag=norm_tag, unify_rejections=True
)
df_resampled = resample_redshift_cosmologically_from_results(
    results,
    cosmological_seed=cosmological_seed,
    resampling_seed=resampling_seed,
    parallel=False,
    debug=debug,
)
# data in processed format but now (1) filtered for only unified successes, (2) only including redshift above 0.02, and (3) sorted by redshift instead of injection index
df_resampled.to_hdf(
    resampled_filename, key="df", format="table", min_itemsize={"rejection_flag": 40}
)
