#!/bin/bash
#
#SBATCH --job-name=mprofUnifiedInjs
#SBATCH --output=slurm_output_files/stdout_job_memory_profile_unified_injections_JOB-ID_%A_TASK-ID_%a.txt
#SBATCH --error=slurm_output_files/stderr_job_memory_profile_unified_injections_JOB-ID_%A_TASK-ID_%a.txt
#
#SBATCH --ntasks=1
#SBATCH --time=6-0 # 6 day 0 hr #72:00:00 # HH:MM:SS
#SBATCH --mem-per-cpu=1000 # MB
#
#SBATCH --array=1,3

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Slurm script to profile the memory usage of the processing unified injections script.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
# License:
#     BSD 3-Clause License
# 
#     Copyright (c) 2022, James Gardner.
#     All rights reserved except for those for the gwbench code which remain reserved
#     by S. Borhanian; the gwbench code is included in this repository for convenience.
# 
#     Redistribution and use in source and binary forms, with or without
#     modification, are permitted provided that the following conditions are met:
# 
#     1. Redistributions of source code must retain the above copyright notice, this
#        list of conditions and the following disclaimer.
# 
#     2. Redistributions in binary form must reproduce the above copyright notice,
#        this list of conditions and the following disclaimer in the documentation
#        and/or other materials provided with the distribution.
# 
#     3. Neither the name of the copyright holder nor the names of its
#        contributors may be used to endorse or promote products derived from
#        this software without specific prior written permission.
# 
#     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#     AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#     IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#     FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#     SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#     CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#     OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#     OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

TEST_INDS=(1 1024 1025 2048)
# slurm uses a base index of 1 here but bash uses 0
let "TEST_IND = ${TEST_INDS[$SLURM_ARRAY_TASK_ID - 1]}"

srun mprof run -o "./slurm_job_scripts/memory_profiling/mprof_plot_unified_injs_${TEST_IND}.dat" ./run_as_task_calculate_unified_injections.py ${TEST_IND}; mprof plot -o "./slurm_job_scripts/memory_profiling/mprof_plot_unified_injs_${TEST_IND}.pdf" "./slurm_job_scripts/memory_profiling/mprof_plot_unified_injs_${TEST_IND}.dat"

# experimented with running jobs with half the file, shuffled or unshuffled. As of August 2022, settled on running one science case at a time since the networks are different, running the whole injections file, shuffling it between all the 2048 tasks, and discarding the remainder. This is limited by the QoS 1000 core per user limit and any inefficiencies of the code/python.

