#!/bin/bash
#
#SBATCH --job-name=setup
#SBATCH --output=slurm_output_files/stdout_job_set-up_for_calculating_injections_JOB-ID_%j.txt
#SBATCH --error=slurm_output_files/stderr_job_set-up_for_calculating_injections_JOB-ID_%j.txt
#
#SBATCH --ntasks=1
#SBATCH --time=00:10:00 # HH:MM:SS
#SBATCH --mem-per-cpu=2000 # MB, generate_injections.py uses 2.3 min and 800 MB when testing but 1000 MB causes out-of-memory error?

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Slurm script to set up for processing the injections. Called by job_main.sh.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
# License:
#     BSD 3-Clause License
# 
#     Copyright (c) 2022, James Gardner.
#     All rights reserved except for those for the gwbench code which remain reserved
#     by S. Borhanian; the gwbench code is included in this repository for convenience.
# 
#     Redistribution and use in source and binary forms, with or without
#     modification, are permitted provided that the following conditions are met:
# 
#     1. Redistributions of source code must retain the above copyright notice, this
#        list of conditions and the following disclaimer.
# 
#     2. Redistributions in binary form must reproduce the above copyright notice,
#        this list of conditions and the following disclaimer in the documentation
#        and/or other materials provided with the distribution.
# 
#     3. Neither the name of the copyright holder nor the names of its
#        contributors may be used to endorse or promote products derived from
#        this software without specific prior written permission.
# 
#     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#     AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#     IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#     FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#     SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#     CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#     OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#     OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# getting arguments through sbatch via <https://stackoverflow.com/a/31602792>
# TODO: repair main job script
#echo "Job ID's to not delete outputs .txt's from:" ${main_job_id} ${SLURM_JOB_ID}
srun bash ./delete_all_data_files.sh ${main_job_id} ${SLURM_JOB_ID} &&
# srun bash ./delete_all_data_files.sh 9999 ${SLURM_JOB_ID} &&
srun python3 ./generate_PSDs.py &&
# uncomment the line below if symbolic derivatives need generating
# srun python3 ./generate_symbolic_derivatives.py &&
srun python3 ./generate_injections.py
