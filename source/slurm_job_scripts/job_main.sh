#!/bin/bash
#
#SBATCH --job-name=main
#SBATCH --output=slurm_output_files/stdout_job_main_JOB-ID_%j.txt
#SBATCH --error=slurm_output_files/stderr_job_main_JOB-ID_%j.txt
#
#SBATCH --ntasks=1
#SBATCH --time=0:01:00 # HH:MM:SS, time to set jobs but not to wait for them to start
#SBATCH --mem-per-cpu=100 # MB, surely less than 1MB is required? TODO: test this.

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Slurm script to run the entire standard analysis pipeline.
#
# Creates sub--slurm jobs for generating injections, calculating results, merging and resampling results, and plotting.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
# License:
#     BSD 3-Clause License
# 
#     Copyright (c) 2022, James Gardner.
#     All rights reserved except for those for the gwbench code which remain reserved
#     by S. Borhanian; the gwbench code is included in this repository for convenience.
# 
#     Redistribution and use in source and binary forms, with or without
#     modification, are permitted provided that the following conditions are met:
# 
#     1. Redistributions of source code must retain the above copyright notice, this
#        list of conditions and the following disclaimer.
# 
#     2. Redistributions in binary form must reproduce the above copyright notice,
#        this list of conditions and the following disclaimer in the documentation
#        and/or other materials provided with the distribution.
# 
#     3. Neither the name of the copyright holder nor the names of its
#        contributors may be used to endorse or promote products derived from
#        this software without specific prior written permission.
# 
#     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#     AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#     IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#     FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#     SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#     CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#     OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#     OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# waiting for each job to finish before starting the next; passing main job id to not delete present output files
JOBID1=$(sbatch --parsable --export=main_job_id=${SLURM_JOB_ID} slurm_job_scripts/job_set-up_for_calculating_injections.sh)
JOBID2=$(sbatch --parsable --dependency=afterok:${JOBID1} slurm_job_scripts/job_calculate_unified_injections_BNS.sh)
JOBID3=$(sbatch --parsable --dependency=afterok:${JOBID2} slurm_job_scripts/job_merge_and_cosmologically_resample_processed_data_task_files.sh)
JOBID4=$(sbatch --parsable --dependency=afterok:${JOBID3} slurm_job_scripts/job_plot_detection_rate_and_measurement_errors.sh)
sbatch --dependency=afterok:${JOBID4} --export=job_id_for_sacct=${JOBID2} slurm_job_scripts/job_store_all_information_from_run.sh
