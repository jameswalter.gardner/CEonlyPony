#!/usr/bin/env python3
"""Generate the sensitivity curves for the NEMO-Lite configuration and save them for gwbench to use.

Truncates the ASD in 'a_plus.txt' to only include those audio-band frequencies where four-stage suspension is not required to achieve the sensitivity, e.g. to only above 100--200 Hz. Saves the result in './gwbench/noise_curves'.
NEMO curve is from <https://dcc.ligo.org/LIGO-T2000062>.

Usage:
    $ python3 generate_PSDs.py
    TODO: Modify gwbench's psd.py to be able to use these new PSDs whenever updated.

License:
    BSD 3-Clause License

    Copyright (c) 2022, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import numpy as np
from pandas import read_csv
from copy import copy
from typing import List, Set, Dict, Tuple, Optional, Union, Any


def generate_high_pass_cuts_of_ASD(
    input_filename_with_path: str,
    output_filetag_with_path: str,
    frequency_cuts: List[float],
    input_in_PSD: bool = False,
    output_in_PSD: bool = True,
    rolloff: bool = False,
    rolloff_orders: List[float] = [
        -6.0,
    ],
    save_uncut_file: bool = False,
    save_rolloff_curve: bool = False,
) -> None:
    """Generates high passed cuts of a given ASD above the given frequency values as PSD.

    Args:
        input_filename_with_path: Targeted ASD .txt file with frequencies as the first column and ASD sensitivity (NSR) in the second column.
        output_filetag_with_path: File tag to add cut note to and save into gwbench for simulation.
        frequency_cuts: Iterable of frequency cuts below which the sensitivity is set to some negligible value, e.g. 1.
        input_in_PSD: Whether the input needs to be converted to PSD, e.g. for NEMO.
        output_in_PSD: Whether to make the output PSD.
        rolloff: Whether to rolloff below the cutoff frequency.
        rolloff_order: What order, i.e. x in f^x, to rolloff at, e.g. rolloff=False is x=-infinity. Performs the rolloff in ASD.
        save_uncut_file: Whether to also save the uncut sensitivity.
        save_rolloff_curve: Whether to also save the rolloff curve for debugging.
    """
    #     data = read_csv(path_aplus + input_filename_aplus, sep=None, header=None, engine="python", comment="#").to_numpy()
    data = np.genfromtxt(input_filename_with_path)

    if save_uncut_file:
        uncut_data = copy(data)
        if output_in_PSD and not input_in_PSD:
            uncut_data[:, 1] = uncut_data[:, 1] ** 2
        elif not output_in_PSD and input_in_PSD:
            uncut_data[:, 1] = uncut_data[:, 1] ** 0.5
        np.savetxt(f"{output_filetag_with_path}.txt", uncut_data)

    for cut in frequency_cuts:
        for rolloff_order in rolloff_orders:
            cut_data = copy(data)
            if not rolloff:
                cut_filetag = f"_above_{cut:.0f}Hz.txt"
                # some arbitrary value much higher (worse) than usual 1e-17 sensitivity
                fill_value = 1.0
                # don't just cut the frequency axis as we want to analyse those frequencies for other detectors and multi-network uses a unified frequency
                cut_data[cut_data[:, 0] < cut, 1] = np.full_like(
                    cut_data[cut_data[:, 0] < cut, 1], fill_value
                )
            else:
                cut_filetag = f"_f-{-rolloff_order:.0f}_rolloff_below_{cut:.0f}Hz.txt"
                # TODO: deal with overflow when cut is high
                #                 rolloff_data = np.array(list(map(
                #                     lambda f : sens_at_cut*(f/cut)**rolloff_order,
                #                     cut_data[cut_data[:, 0] < cut, 0]
                #                 )))
                #                 cut_data[cut_data[:, 0] < cut, 1] = rolloff_data
                # rolloff in ASD
                if input_in_PSD:
                    cut_data[:, 1] = cut_data[:, 1] ** 0.5
                #                 print(f"debug: {output_filetag_with_path}, {cut} Hz, f^{rolloff_order}, {cut_data}")
                sens_at_cut = cut_data[cut_data[:, 0].searchsorted(cut), 1]
                rolloff_data = copy(cut_data)
                rolloff_data[:, 1] = np.array(
                    list(
                        map(
                            lambda frequency: sens_at_cut
                            * (frequency / cut) ** rolloff_order,
                            cut_data[:, 0],
                        )
                    )
                )
                #                 print(f"debug : {cut} Hz, {sens_at_cut}, {rolloff_data}, {cut_data}")
                # add existing curve to rolloff in quadrature
                cut_data[:, 1] = np.sqrt(cut_data[:, 1] ** 2 + rolloff_data[:, 1] ** 2)
                # TODO: unify filetags between gwbench I/O and variable/plot display of detectors
                # undo rolloff in ASD
                if input_in_PSD:
                    cut_data[:, 1] = cut_data[:, 1] ** 2
                if save_rolloff_curve:
                    if output_in_PSD:
                        rolloff_data[:, 1] = rolloff_data[:, 1] ** 2
                    np.savetxt(
                        output_filetag_with_path
                        + cut_filetag.replace(".txt", "_rolloff_reference.txt"),
                        rolloff_data,
                    )

            # check whether data needs converting to PSD or ASD
            if output_in_PSD and not input_in_PSD:
                cut_data[:, 1] = cut_data[:, 1] ** 2
            elif not output_in_PSD and input_in_PSD:
                cut_data[:, 1] = cut_data[:, 1] ** 0.5
            np.savetxt(output_filetag_with_path + cut_filetag, cut_data)


NOISE_CURVE_PATH_GWBENCH = "./gwbench/noise_curves/"
NOISE_CURVE_ADDITIONAL_FILES = "./noise_curves_to_add_to_gwbench/"

# from Bram Aug 19 2022: Can we do a 10, 50, 100, 200 Hz cut offs [of A+]? ... You could even add a 500 Hz for the actual NEMO config.  But that is close enough to the 200 Hz cut
# 10 Hz is already the LF min of the A+ curve, so leave that out.
generate_high_pass_cuts_of_ASD(
    NOISE_CURVE_PATH_GWBENCH + "a_plus.txt",
    NOISE_CURVE_PATH_GWBENCH + "psd_a_plus",
    (50, 100, 200),
    input_in_PSD=False,
    output_in_PSD=True,
)
generate_high_pass_cuts_of_ASD(
    NOISE_CURVE_ADDITIONAL_FILES + "NEMO_PSD_NSR.txt",
    NOISE_CURVE_PATH_GWBENCH + "psd_nemo",
    (500,),
    input_in_PSD=True,
    output_in_PSD=True,
)
generate_high_pass_cuts_of_ASD(
    NOISE_CURVE_ADDITIONAL_FILES + "A#_ASD_NSR.txt",
    NOISE_CURVE_PATH_GWBENCH + "psd_a_sharp",
    (50, 100, 200),
    input_in_PSD=False,
    output_in_PSD=True,
    rolloff=True,
    rolloff_orders=[-6, -8],
    save_uncut_file=True,
    save_rolloff_curve=True,
)
generate_high_pass_cuts_of_ASD(
    NOISE_CURVE_ADDITIONAL_FILES + "NEMO_PSD_NSR.txt",
    NOISE_CURVE_PATH_GWBENCH + "psd_nemo",
    (500,),
    input_in_PSD=True,
    output_in_PSD=True,
    rolloff=True,
    rolloff_orders=[-6, -8],
    save_uncut_file=True,
)
