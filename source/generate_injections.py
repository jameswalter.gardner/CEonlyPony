#!/usr/bin/env python3
"""Generates astrophysical and observational parameters of injections and saves them as .h5.

Based on old calculate_injections.py.

Usage:
    $ python3 generate_injections.py

License:
    BSD 3-Clause License

    Copyright (c) 2022, James Gardner.
    All rights reserved except for those for the gwbench code which remain reserved
    by S. Borhanian; the gwbench code is included in this repository for convenience.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
from typing import List, Set, Dict, Tuple, Optional, Union
import numpy as np
import pandas as pd
import glob

from gwbench import injections


def inj_params_for_science_case(
    science_case: str,
) -> Tuple[
    Dict[str, Union[str, float, int]],
    Dict[str, Union[str, float, int]],
    Tuple[Tuple[float, float, float], ...],
    int,
]:
    """Returns the injection parameters to pass to generate_injections for a given science case.

    Args:
        science_case: Science case.

    Raises:
        ValueError: If science case is not recognised.
    """
    if science_case == "BNS":
        # injection settings - source
        mass_dict = dict(dist="gaussian", mean=1.35, sigma=0.15, mmin=1, mmax=2)
        spin_dict = dict(geom="cartesian", dim=1, chi_lo=-0.05, chi_hi=0.05)
        # redshift_bins = ((zmin, zmax, seed), ...) (use same seeds from B&S2022 to replicate results)
        # using 0 from AppA and Ssohrab rather than 0.02 from main text
        redshift_bins = (
            (0, 0.5, 7669),
            (0.5, 1, 3103),
            (1, 2, 4431),
            (2, 4, 5526),
            (4, 10, 7035),
            (10, 50, 2785),
        )
        coeff_fisco = 4  # fmax = 4*fisco for BNS, 8*fisco for BBH
    elif science_case == "BBH":
        # following injection.py and GWTC-2 (AppB.2. Power Law + Peak mass model), TODO: update for GWTC-3?
        # m1 follows power peak, m2 follow uniform in (5 Msun, m1) --> change mmin to 5?
        mass_dict = dict(
            dist="power_peak_uniform",
            mmin=4.59,  # 4.59 in GWTC-2 and Ssohrab's code, paper disagrees.
            mmax=86.22,
            m1_alpha=2.63,
            q_beta=1.26,
            peak_frac=0.1,
            peak_mean=33.07,  # assuming that peak_mu is peak_mean?
            peak_sigma=5.69,
            delta_m=4.82,
        )
        spin_dict = dict(geom="cartesian", dim=1, chi_lo=-0.75, chi_hi=0.75)
        redshift_bins = (
            (0, 0.5, 5485),
            (0.5, 1, 1054),
            (1, 2, 46),
            (2, 4, 5553),
            (4, 10, 5998),
            (10, 50, 4743),
        )
        coeff_fisco = 8
    else:
        raise ValueError("Science case not recognised.")
    return mass_dict, spin_dict, redshift_bins, coeff_fisco


def injection_file_name(
    science_case: str, num_injs_per_redshift_bin: int, task_id: Optional[int] = None
) -> str:
    """Returns the file name for the raw injection data without path.

    Args:
        science_case: Science case.
        num_injs_per_redshift_bin: Number of injections per redshift major bin.
        task_id: Task ID.
    """
    file_name = f"SCI-CASE_{science_case}_INJS-PER-ZBIN_{num_injs_per_redshift_bin}.h5"
    if task_id is not None:
        file_name = file_name.replace(".h5", f"_TASK_{task_id}.h5")
    return file_name


def generate_injections(
    num_injs_per_redshift_bin: int,
    redshift_bins: Tuple[Tuple[float, float, float], ...],
    mass_dict: Dict[str, Union[str, float, int]],
    spin_dict: Dict[str, Union[str, float, int]],
    redshifted: bool,
    coeff_fisco: int,
    science_case: str,
    inj_data_path: str = "./data_raw_injections/",
) -> None:
    """Generates raw injections data sampled uniformly linearly in redshift, saves as a pandas dataframe in .h5 format.

    Args:
        num_injs_per_redshift_bin: Number of redshift major bins.
        redshift_bins: Redshift bins in the form ((minimum redshift, maximum redshift, random seed, ...).
        mass_dict: Injection settings for mass sampler.
        spin_dict: Injection settings for spin sampler.
        redshifted: Whether gwbench should redshift the masses.
        coeff_fisco: Coefficient of frequency of ISCO.
        science_case: Science case.
        inj_data_path: Path to output directory.
    """
    inj_file_name = inj_data_path + injection_file_name(
        science_case, num_injs_per_redshift_bin
    )
    #     # initially [Mc, eta, chi1x, chi1y, chi1z, chi2x, chi2y, chi2z, DL, iota, ra, dec, psi, z]
    with pd.HDFStore(inj_file_name) as store:
        for i, (zmin, zmax, seed) in enumerate(redshift_bins):
            cosmo_dict = dict(sampler="uniform", zmin=zmin, zmax=zmax)
            injection_df_in_bin = pd.DataFrame(
                np.array(
                    injections.injections_CBC_params_redshift(
                        cosmo_dict,
                        mass_dict,
                        spin_dict,
                        redshifted,
                        num_injs=num_injs_per_redshift_bin,
                        seed=seed,
                    )
                ).transpose(),
                columns=[
                    "Mc",
                    "eta",
                    "chi1x",
                    "chi1y",
                    "chi1z",
                    "chi2x",
                    "chi2y",
                    "chi2z",
                    "DL",
                    "iota",
                    "ra",
                    "dec",
                    "psi",
                    "z",
                ],
            )
            store.append(key="df", value=injection_df_in_bin, format="table")

        injection_df = store.get(key="df")
        # labelling each injection to keep track of them; injection_index is now the first columns
        # filtering for fmax is now done later
        injection_df.insert(
            loc=0, column="injection_index", value=range(len(injection_df))
        )
        store.put(key="df", value=injection_df)


def chop_injections_data_for_processing(
    job_array_size: int = 4096,
    inj_data_path: str = "./data_raw_injections/",
    output_data_path: str = "./data_raw_injections/task_files/",
    shuffle_rows: bool = True,
    random_seed: Optional[int] = None,
    discard_remainder: bool = True,
) -> None:
    """Splits (chops) the saved injections data into different files for each of the parallel tasks later to run over.

    Splits the task evenly between each task and science case.

    Args:
        job_array_size: Number of tasks in slurm job array.
        inj_data_path: Path to input injections data.
        output_data_path: Path to output (chopped) injections data.
        shuffle_rows: Whether to shuffle the rows before chopping, needs sorting in merging script.
        random_seed: Random seed for shuffling of rows.
        discard_remainder: Whether to drop the remaining tasks after evenly distributing or to pile them into the last script.
    """
    files = [file for file in sorted(glob.glob(inj_data_path + "*.h5"))]
    num_science_cases = len(files)
    # TODO: more efficiently allocate the tasks, currently is 1464 in each task and more in the last of each science case to pick up the remainder. This effect should be at most adding 1023 tasks to the last job which will be roughly 1.7 times as long.
    tasks_per_sc = job_array_size // num_science_cases
    for i, file in enumerate(files):
        # absolute path included
        science_case, num_injs_per_redshift_bin_str = (
            file.replace("_INJS-PER-ZBIN_", "SCI-CASE_")
            .replace(".h5", "SCI-CASE_")
            .split("SCI-CASE_")[1:3]
        )
        num_injs_per_redshift_bin = int(num_injs_per_redshift_bin_str)
        injection_df = pd.read_hdf(file)
        if shuffle_rows:
            injection_df = injection_df.sample(
                frac=1, random_state=random_seed
            ).reset_index(drop=True)
        injs_per_task = len(injection_df) // tasks_per_sc
        chop_inds = [
            (j * injs_per_task, (j + 1) * injs_per_task) for j in range(tasks_per_sc)
        ]
        if not discard_remainder:
            # extend last task to cover any remainder after // above
            chop_inds[-1] = (chop_inds[-1][0], None)
        else:
            print(
                f"Dropped the remaining {len(injection_df) - injs_per_task * tasks_per_sc} tasks."
            )
            # TODO: output the dropped injections somewhere to check for accidental bias? shuffling a large set should be sufficient
        for j in range(tasks_per_sc):
            # using convention that task ID is one indexed
            task_id = i * tasks_per_sc + j + 1
            task_file_name = output_data_path + injection_file_name(
                science_case, num_injs_per_redshift_bin, task_id=task_id
            )
            chopped_df = injection_df.iloc[chop_inds[j][0] : chop_inds[j][1]]
            chopped_df.to_hdf(task_file_name, key="df", format="table")


if __name__ == "__main__":
    # 1.5M total injections to match B&S2022
    num_injs_per_redshift_bin = 250000
    science_cases = ("BBH", "BNS")
    redshifted = True
    shuffle_rows = True
    random_seed = 67890

    for science_case in science_cases:
        mass_dict, spin_dict, redshift_bins, coeff_fisco = inj_params_for_science_case(
            science_case
        )
        generate_injections(
            num_injs_per_redshift_bin,
            redshift_bins,
            mass_dict,
            spin_dict,
            redshifted,
            coeff_fisco,
            science_case,
        )

    chop_injections_data_for_processing(
        shuffle_rows=shuffle_rows, random_seed=random_seed
    )
